<?php
	/* a template for displaying the header area */
?>	
<header class="chariti-header-wrap chariti-header-style-side-toggle" >
	<?php
		$display_logo = chariti_get_option('general', 'header-side-toggle-display-logo', 'enable');
		if( $display_logo == 'enable' ){
			echo chariti_get_logo(array('padding' => false));
		}

		$navigation_class = '';
		if( chariti_get_option('general', 'enable-main-navigation-submenu-indicator', 'disable') == 'enable' ){
			$navigation_class = 'chariti-navigation-submenu-indicator ';
		}
	?>
	<div class="chariti-navigation clearfix <?php echo esc_attr($navigation_class); ?>" >
	<?php
		// print main menu
		if( has_nav_menu('main_menu') ){
			chariti_get_custom_menu(array(
				'container-class' => 'chariti-main-menu',
				'button-class' => 'chariti-side-menu-icon',
				'icon-class' => 'fa fa-bars',
				'id' => 'chariti-main-menu',
				'theme-location' => 'main_menu',
				'type' => chariti_get_option('general', 'header-side-toggle-menu-type', 'overlay')
			));
		}
	?>
	</div><!-- chariti-navigation -->
	<?php

		// menu right side
		$enable_search = (chariti_get_option('general', 'enable-main-navigation-search', 'enable') == 'enable')? true: false;
		$enable_cart = (chariti_get_option('general', 'enable-main-navigation-cart', 'enable') == 'enable' && class_exists('WooCommerce'))? true: false;
		if( $enable_search || $enable_cart ){ 
			echo '<div class="chariti-header-icon chariti-pos-bottom" >';

			// search icon
			if( $enable_search ){
				$search_icon = chariti_get_option('general', 'main-navigation-search-icon', 'fa fa-search');
				echo '<div class="chariti-main-menu-search" id="chariti-top-search" >';
				echo '<i class="' . esc_attr($search_icon) . '" ></i>';
				echo '</div>';
				chariti_get_top_search();
			}

			// cart icon
			if( $enable_cart ){
				$cart_icon = chariti_get_option('general', 'main-navigation-cart-icon', 'fa fa-shopping-cart');
				echo '<div class="chariti-main-menu-cart" id="chariti-main-menu-cart" >';
				echo '<i class="' . esc_attr($cart_icon) . '" data-chariti-lb="top-bar" ></i>';
				chariti_get_woocommerce_bar();
				echo '</div>';
			}

			echo '</div>'; // chariti-main-menu-right-wrap
		}

	?>
</header><!-- header -->