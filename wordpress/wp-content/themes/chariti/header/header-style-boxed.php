<?php
	/* a template for displaying the header area */

	// header container
	$header_width = chariti_get_option('general', 'header-width', 'boxed');
	
	if( $header_width == 'boxed' ){
		$header_container_class = ' chariti-container';
	}else if( $header_width == 'custom' ){
		$header_container_class = ' chariti-header-custom-container';
	}else{
		$header_container_class = ' chariti-header-full';
	}

	$header_style = chariti_get_option('general', 'header-boxed-style', 'menu-right');
	$navigation_offset = chariti_get_option('general', 'fixed-navigation-anchor-offset', '');

	$header_wrap_class  = ' chariti-style-' . $header_style;
	$header_wrap_class .= ' chariti-sticky-navigation chariti-style-slide';
?>	
<header class="chariti-header-wrap chariti-header-style-boxed <?php echo esc_attr($header_wrap_class); ?>" <?php
		if( !empty($navigation_offset) ){
			echo 'data-navigation-offset="' . esc_attr($navigation_offset) . '" ';
		}
	?> >
	<div class="chariti-header-container clearfix <?php echo esc_attr($header_container_class); ?>">
		<div class="chariti-header-container-inner clearfix">	

			<div class="chariti-header-background  chariti-item-mglr" ></div>
			<div class="chariti-header-container-item clearfix">
				<?php

					if( $header_style == 'splitted-menu' ){
						global $chariti_center_nav_item;
						$chariti_center_nav_item = chariti_get_logo();
					}else{
						echo chariti_get_logo();
					}

					$navigation_class = '';
					if( chariti_get_option('general', 'enable-main-navigation-submenu-indicator', 'disable') == 'enable' ){
						$navigation_class = 'chariti-navigation-submenu-indicator ';
					}
				?>
				<div class="chariti-navigation chariti-item-pdlr clearfix <?php echo esc_attr($navigation_class); ?>" >
				<?php
					// print main menu
					if( has_nav_menu('main_menu') ){
						echo '<div class="chariti-main-menu" id="chariti-main-menu" >';
						wp_nav_menu(array(
							'theme_location'=>'main_menu', 
							'container'=> '', 
							'menu_class'=> 'sf-menu',
							'walker' => new chariti_menu_walker()
						));
						
						chariti_get_navigation_slide_bar();
						
						echo '</div>';
					}

					if( $header_style == 'splitted-menu' ){
						$chariti_center_nav_item = '';
					}

					// menu right side
					$menu_right_class = '';
					if( $header_style == 'center-menu' || $header_style == 'splitted-menu' ){
						$menu_right_class = ' chariti-item-mglr chariti-navigation-top chariti-navigation-right';
					}

					$enable_search = (chariti_get_option('general', 'enable-main-navigation-search', 'enable') == 'enable')? true: false;
					$enable_cart = (chariti_get_option('general', 'enable-main-navigation-cart', 'enable') == 'enable' && class_exists('WooCommerce'))? true: false;
					$enable_right_button = (chariti_get_option('general', 'enable-main-navigation-right-button', 'disable') == 'enable')? true: false;
					if( has_nav_menu('right_menu') || $enable_search || $enable_cart || $enable_right_button ){
						echo '<div class="chariti-main-menu-right-wrap clearfix ' . esc_attr($menu_right_class) . '" >';

						// search icon
						if( $enable_search ){
							$search_icon = chariti_get_option('general', 'main-navigation-search-icon', 'fa fa-search');
							echo '<div class="chariti-main-menu-search" id="chariti-top-search" >';
							echo '<i class="' . esc_attr($search_icon) . '" ></i>';
							echo '</div>';
							chariti_get_top_search();
						}

						// cart icon
						if( $enable_cart ){
							$cart_icon = chariti_get_option('general', 'main-navigation-cart-icon', 'fa fa-shopping-cart');
							echo '<div class="chariti-main-menu-cart" id="chariti-main-menu-cart" >';
							echo '<i class="' . esc_attr($cart_icon) . '" data-chariti-lb="top-bar" ></i>';
							chariti_get_woocommerce_bar();
							echo '</div>';
						}

						// menu right button
						if( $enable_right_button ){
							$button_class = 'chariti-style-' . chariti_get_option('general', 'main-navigation-right-button-style', 'default');
							$button_link = chariti_get_option('general', 'main-navigation-right-button-link', '');
							$button_link_target = chariti_get_option('general', 'main-navigation-right-button-link-target', '_self');
							if( !empty($button_link) ){
								echo '<a class="chariti-main-menu-right-button chariti-button-1 ' . esc_attr($button_class) . '" href="' . esc_url($button_link) . '" target="' . esc_attr($button_link_target) . '" >';
								echo chariti_get_option('general', 'main-navigation-right-button-text', '');
								echo '</a>';
							}
							
							$button_class = 'chariti-style-' . chariti_get_option('general', 'main-navigation-right-button-style-2', 'default');
							$button_link = chariti_get_option('general', 'main-navigation-right-button-link-2', '');
							$button_link_target = chariti_get_option('general', 'main-navigation-right-button-link-target-2', '_self');
							if( !empty($button_link) ){
								echo '<a class="chariti-main-menu-right-button chariti-button-2 ' . esc_attr($button_class) . '" href="' . esc_url($button_link) . '" target="' . esc_attr($button_link_target) . '" >';
								echo chariti_get_option('general', 'main-navigation-right-button-text-2', '');
								echo '</a>';
							}
						}

						// print right menu
						if( has_nav_menu('right_menu') && $header_style != 'splitted-menu' ){
							chariti_get_custom_menu(array(
								'container-class' => 'chariti-main-menu-right',
								'button-class' => 'chariti-right-menu-button chariti-top-menu-button',
								'icon-class' => 'fa fa-bars',
								'id' => 'chariti-right-menu',
								'theme-location' => 'right_menu',
								'type' => chariti_get_option('general', 'right-menu-type', 'right')
							));
						}

						echo '</div>'; // chariti-main-menu-right-wrap

						if( has_nav_menu('right_menu') && $header_style == 'splitted-menu' ){
							echo '<div class="chariti-main-menu-left-wrap clearfix chariti-item-pdlr chariti-navigation-top chariti-navigation-left" >';
							chariti_get_custom_menu(array(
								'container-class' => 'chariti-main-menu-right',
								'button-class' => 'chariti-right-menu-button chariti-top-menu-button',
								'icon-class' => 'fa fa-bars',
								'id' => 'chariti-right-menu',
								'theme-location' => 'right_menu',
								'type' => chariti_get_option('general', 'right-menu-type', 'right')
							));
							echo '</div>';
						}
					}
				?>
				</div><!-- chariti-navigation -->

			</div><!-- chariti-header-container-inner -->
		</div><!-- chariti-header-container-item -->
	</div><!-- chariti-header-container -->
</header><!-- header -->