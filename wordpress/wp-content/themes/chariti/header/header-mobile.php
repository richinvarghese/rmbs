<?php
	// mobile menu template
	echo '<div class="chariti-mobile-header-wrap" >';

	// top bar
	$top_bar = chariti_get_option('general', 'enable-top-bar-on-mobile', 'disable');
	if( $top_bar == 'enable' ){
		get_template_part('header/header', 'top-bar');
	}

	// header
	$logo_position = chariti_get_option('general', 'mobile-logo-position', 'logo-left');
	$sticky_mobile_nav = chariti_get_option('general', 'enable-mobile-navigation-sticky', 'enable');
	echo '<div class="chariti-mobile-header chariti-header-background chariti-style-slide ';
	if($sticky_mobile_nav == 'enable'){
		echo 'chariti-sticky-mobile-navigation ';
	}
	echo '" id="chariti-mobile-header" >';
	echo '<div class="chariti-mobile-header-container chariti-container clearfix" >';
	echo chariti_get_logo(array(
		'mobile' => true,
		'wrapper-class' => ($logo_position == 'logo-center'? 'chariti-mobile-logo-center': '')
	));

	echo '<div class="chariti-mobile-menu-right" >';

	// search icon
	$enable_search = (chariti_get_option('general', 'enable-main-navigation-search', 'enable') == 'enable')? true: false;
	if( $enable_search ){
		echo '<div class="chariti-main-menu-search" id="chariti-mobile-top-search" >';
		echo '<i class="fa fa-search" ></i>';
		echo '</div>';
		chariti_get_top_search();
	}

	// cart icon
	$enable_cart = (chariti_get_option('general', 'enable-main-navigation-cart', 'enable') == 'enable' && class_exists('WooCommerce'))? true: false;
	if( $enable_cart ){
		echo '<div class="chariti-main-menu-cart" id="chariti-mobile-menu-cart" >';
		echo '<i class="fa fa-shopping-cart" data-chariti-lb="top-bar" ></i>';
		chariti_get_woocommerce_bar();
		echo '</div>';
	}

	if( $logo_position == 'logo-center' ){
		echo '</div>';
		echo '<div class="chariti-mobile-menu-left" >';
	}

	// mobile menu
	if( has_nav_menu('mobile_menu') ){
		chariti_get_custom_menu(array(
			'type' => chariti_get_option('general', 'right-menu-type', 'right'),
			'container-class' => 'chariti-mobile-menu',
			'button-class' => 'chariti-mobile-menu-button',
			'icon-class' => 'fa fa-bars',
			'id' => 'chariti-mobile-menu',
			'theme-location' => 'mobile_menu'
		));
	}
	echo '</div>'; // chariti-mobile-menu-right/left
	echo '</div>'; // chariti-mobile-header-container
	echo '</div>'; // chariti-mobile-header

	echo '</div>'; // chariti-mobile-header-wrap