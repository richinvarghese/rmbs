<?php
	/* a template for displaying the header area */

	// header container
	$body_layout = chariti_get_option('general', 'layout', 'full');
	$body_margin = chariti_get_option('general', 'body-margin', '0px');
	$header_width = chariti_get_option('general', 'header-width', 'boxed');
	$header_style = chariti_get_option('general', 'header-bar2-navigation-align', 'center');
	$header_background_style = chariti_get_option('general', 'header-background-style', 'solid');

	$header_wrap_class = '';
	if( $header_style == 'center-logo' ){
		$header_wrap_class .= ' chariti-style-center';
	}else{
		$header_wrap_class .= ' chariti-style-left';
	}

	$header_container_class = '';
	if( $header_width == 'boxed' ){
		$header_container_class .= ' chariti-container';
	}else if( $header_width == 'custom' ){
		$header_container_class .= ' chariti-header-custom-container';
	}else{
		$header_container_class .= ' chariti-header-full';
	}

	$navigation_wrap_class  = ' chariti-sticky-navigation chariti-sticky-navigation-height';
	if( $header_style == 'center' || $header_style == 'center-logo' ){
		$navigation_wrap_class .= ' chariti-style-center';
	}else{
		$navigation_wrap_class .= ' chariti-style-left';
	}
	if( $body_layout == 'boxed' || $body_margin != '0px' ){
		$navigation_wrap_class .= ' chariti-style-slide';
	}else{
		$navigation_wrap_class .= '  chariti-style-fixed';
	}

?>	
<header class="chariti-header-wrap chariti-header-style-bar chariti-style-2 <?php echo esc_attr($header_wrap_class); ?>" >
	<div class="chariti-header-background"></div>
	<div class="chariti-header-container clearfix <?php echo esc_attr($header_container_class); ?>">
		<div class="chariti-header-container-inner">
		<?php
			
			echo chariti_get_logo();

			$logo_right_text = '';
			for( $i=1; $i<=3; $i++ ){
				$icon = chariti_get_option('general', 'logo-right-box' . $i . '-icon', '');
				$title = chariti_get_option('general', 'logo-right-box' . $i . '-title', '');
				$caption = chariti_get_option('general', 'logo-right-box' . $i . '-caption', '');

				if( !empty($icon) || !empty($title) || !empty($caption) ){
					$logo_right_text .= '<div class="chariti-logo-right-text-wrap" >';
					$logo_right_text .= '<i class="chariti-logo-right-text-icon ' . esc_attr($icon) . '" ></i>';
					$logo_right_text .= '<div class="chariti-logo-right-text-content-wrap" >';
					if( !empty($title) ){
						$logo_right_text .= '<div class="chariti-logo-right-text-title" >' . gdlr_core_text_filter($title) . '</div>';
					}
					if( !empty($caption) ){
						$logo_right_text .= '<div class="chariti-logo-right-text-caption" >' . gdlr_core_text_filter($caption) . '</div>';
					}
					$logo_right_text .= '</div>';
					$logo_right_text .= '</div>';
				}
			}

			// menu right button
			$enable_right_button = (chariti_get_option('general', 'enable-main-navigation-right-button', 'disable') == 'enable')? true: false;
			if( $enable_right_button ){
				$button_class = 'chariti-style-' . chariti_get_option('general', 'main-navigation-right-button-style', 'default');
				$button_link = chariti_get_option('general', 'main-navigation-right-button-link', '');
				$button_link_target = chariti_get_option('general', 'main-navigation-right-button-link-target', '_self');
				if( !empty($button_link) ){
					$logo_right_text .= '<a class="chariti-main-menu-right-button chariti-button-1 ' . esc_attr($button_class) . '" href="' . esc_url($button_link) . '" target="' . esc_attr($button_link_target) . '" >';
					$logo_right_text .= chariti_get_option('general', 'main-navigation-right-button-text', '');
					$logo_right_text .= '</a>';
				}
			
				$button_class = 'chariti-style-' . chariti_get_option('general', 'main-navigation-right-button-style-2', 'default');
				$button_link = chariti_get_option('general', 'main-navigation-right-button-link-2', '');
				$button_link_target = chariti_get_option('general', 'main-navigation-right-button-link-target-2', '_self');
				if( !empty($button_link) ){
					$logo_right_text .= '<a class="chariti-main-menu-right-button chariti-button-2 ' . esc_attr($button_class) . '" href="' . esc_url($button_link) . '" target="' . esc_attr($button_link_target) . '" >';
					$logo_right_text .= chariti_get_option('general', 'main-navigation-right-button-text-2', '');
					$logo_right_text .= '</a>';
				}
			}

			if( !empty($logo_right_text) ){
				echo '<div class="chariti-logo-right-text chariti-item-pdlr clearfix" >' . gdlr_core_text_filter($logo_right_text) . '</div>';
			}
		?>
		</div>
	</div>
	<div class="chariti-navigation-bar-wrap chariti-navigation-header-style-bar chariti-style-2 <?php echo esc_attr($navigation_wrap_class); ?>" >
		
		<div class="chariti-navigation-container clearfix <?php echo esc_attr($header_container_class); ?>">
			<div class="chariti-navigation-background chariti-item-mglr" ></div>
			<?php
				$navigation_class = '';
				if( chariti_get_option('general', 'enable-main-navigation-submenu-indicator', 'disable') == 'enable' ){
					$navigation_class .= 'chariti-navigation-submenu-indicator ';
				}
			?>
			<div class="chariti-navigation chariti-item-pdlr clearfix <?php echo esc_attr($navigation_class); ?>" >
			<?php
				// print main menu
				if( has_nav_menu('main_menu') ){
					echo '<div class="chariti-main-menu" id="chariti-main-menu" >';
					wp_nav_menu(array(
						'theme_location'=>'main_menu', 
						'container'=> '', 
						'menu_class'=> 'sf-menu',
						'walker' => new chariti_menu_walker()
					));

					chariti_get_navigation_slide_bar();
					
					echo '</div>';
				}

				// menu right side
				$menu_right_class = '';
				if( $header_style == 'center' || $header_style == 'center-logo' ){
					$menu_right_class = ' chariti-item-mglr chariti-navigation-top';
				}

				// menu right side
				$enable_search = (chariti_get_option('general', 'enable-main-navigation-search', 'enable') == 'enable')? true: false;
				$enable_cart = (chariti_get_option('general', 'enable-main-navigation-cart', 'enable') == 'enable' && class_exists('WooCommerce'))? true: false;
				if( has_nav_menu('right_menu') || $enable_search || $enable_cart ){
					echo '<div class="chariti-main-menu-right-wrap clearfix ' . esc_attr($menu_right_class) . '" >';

					// search icon
					if( $enable_search ){
						echo '<div class="chariti-main-menu-search" id="chariti-top-search" >';
						echo '<i class="fa fa-search" ></i>';
						echo '</div>';
						chariti_get_top_search();
					}

					// cart icon
					if( $enable_cart ){
						echo '<div class="chariti-main-menu-cart" id="chariti-main-menu-cart" >';
						echo '<i class="fa fa-shopping-cart" data-chariti-lb="top-bar" ></i>';
						chariti_get_woocommerce_bar();
						echo '</div>';
					}

					// print right menu
					if( has_nav_menu('right_menu') ){
						chariti_get_custom_menu(array(
							'container-class' => 'chariti-main-menu-right',
							'button-class' => 'chariti-right-menu-button chariti-top-menu-button',
							'icon-class' => 'fa fa-bars',
							'id' => 'chariti-right-menu',
							'theme-location' => 'right_menu',
							'type' => chariti_get_option('general', 'right-menu-type', 'right')
						));
					}

					echo '</div>'; // chariti-main-menu-right-wrap
				}
			?>
			</div><!-- chariti-navigation -->

		</div><!-- chariti-header-container -->
	</div><!-- chariti-navigation-bar-wrap -->
</header><!-- header -->