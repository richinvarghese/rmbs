<?php

	add_filter('gdlr_core_enable_header_post_type', 'chariti_gdlr_core_enable_header_post_type');
	if( !function_exists('chariti_gdlr_core_enable_header_post_type') ){
		function chariti_gdlr_core_enable_header_post_type( $args ){
			return true;
		}
	}
	
	add_filter('gdlr_core_header_options', 'chariti_gdlr_core_header_options', 10, 2);
	if( !function_exists('chariti_gdlr_core_header_options') ){
		function chariti_gdlr_core_header_options( $options, $with_default = true ){

			// get option
			$options = array(
				'top-bar' => chariti_top_bar_options(),
				'top-bar-social' => chariti_top_bar_social_options(),			
				'header' => chariti_header_options(),
				'logo' => chariti_logo_options(),
				'navigation' => chariti_navigation_options(), 
				'fixed-navigation' => chariti_fixed_navigation_options(),
			);

			// set default
			if( $with_default ){
				foreach( $options as $slug => $option ){
					foreach( $option['options'] as $key => $value ){
						$options[$slug]['options'][$key]['default'] = chariti_get_option('general', $key);
					}
				}
			} 
			
			return $options;
		}
	}
	
	add_filter('gdlr_core_header_color_options', 'chariti_gdlr_core_header_color_options', 10, 2);
	if( !function_exists('chariti_gdlr_core_header_color_options') ){
		function chariti_gdlr_core_header_color_options( $options, $with_default = true ){

			$options = array(
				'header-color' => chariti_header_color_options(), 
				'navigation-menu-color' => chariti_navigation_color_options(), 		
				'navigation-right-color' => chariti_navigation_right_color_options(),
			);

			// set default
			if( $with_default ){
				foreach( $options as $slug => $option ){
					foreach( $option['options'] as $key => $value ){
						$options[$slug]['options'][$key]['default'] = chariti_get_option('color', $key);
					}
				}
			}

			return $options;
		}
	}

	add_action('wp_head', 'chariti_set_custom_header');
	if( !function_exists('chariti_set_custom_header') ){
		function chariti_set_custom_header(){
			chariti_get_option('general', 'layout', '');

			$header_id = get_post_meta(get_the_ID(), 'gdlr_core_custom_header_id', true);
			if( empty($header_id) ){
				$header_id = chariti_get_option('general', 'custom-header', '');
			}

			if( !empty($header_id) ){
				$option = 'chariti_general';
				$header_options = get_post_meta($header_id, 'gdlr-core-header-settings', true);

				if( !empty($header_options) ){
					foreach( $header_options as $key => $value ){
						$GLOBALS[$option][$key] = $value;
					}
				}

				$header_css = get_post_meta($header_id, 'gdlr-core-custom-header-css', true);
				if( !empty($header_css) ){
					if( get_post_type() == 'page' ){
						$header_css = str_replace('.gdlr-core-page-id', '.page-id-' . get_the_ID(), $header_css);
					}else{
						$header_css = str_replace('.gdlr-core-page-id', '.postid-' . get_the_ID(), $header_css);
					}
					echo '<style type="text/css" >' . $header_css . '</style>';
				}
				

			}
		} // chariti_set_custom_header
	}

	// override menu on page option
	add_filter('wp_nav_menu_args', 'chariti_wp_nav_menu_args');
	if( !function_exists('chariti_wp_nav_menu_args') ){
		function chariti_wp_nav_menu_args($args){

			$chariti_locations = array('main_menu', 'right_menu', 'top_bar_menu', 'mobile_menu');
			if( !empty($args['theme_location']) && in_array($args['theme_location'], $chariti_locations) ){
				$menu_id = get_post_meta(get_the_ID(), 'gdlr-core-location-' . $args['theme_location'], true);
				
				if( !empty($menu_id) ){
					$args['menu'] = $menu_id;
					$args['theme_location'] = '';
				}
			}

			return $args;
		}
	}

	if( !function_exists('chariti_top_bar_options') ){
		function chariti_top_bar_options(){
			return array(
				'title' => esc_html__('Top Bar', 'chariti'),
				'options' => array(

					'enable-top-bar' => array(
						'title' => esc_html__('Enable Top Bar', 'chariti'),
						'type' => 'checkbox',
					),
					'enable-top-bar-on-mobile' => array(
						'title' => esc_html__('Enable Top Bar On Mobile', 'chariti'),
						'type' => 'checkbox',
						'default' => 'disable'
					),
					'top-bar-width' => array(
						'title' => esc_html__('Top Bar Width', 'chariti'),
						'type' => 'combobox',
						'options' => array(
							'boxed' => esc_html__('Boxed ( Within Container )', 'chariti'),
							'full' => esc_html__('Full', 'chariti'),
							'custom' => esc_html__('Custom', 'chariti'),
						),
						'condition' => array( 'enable-top-bar' => 'enable' )
					),
					'top-bar-width-pixel' => array(
						'title' => esc_html__('Top Bar Width Pixel', 'chariti'),
						'type' => 'text',
						'data-type' => 'pixel',
						'default' => '1140px',
						'condition' => array( 'enable-top-bar' => 'enable', 'top-bar-width' => 'custom' ),
						'selector' => '.chariti-top-bar-container.chariti-top-bar-custom-container{ max-width: #gdlr#; }'
					),
					'top-bar-full-side-padding' => array(
						'title' => esc_html__('Top Bar Full ( Left/Right ) Padding', 'chariti'),
						'type' => 'fontslider',
						'data-min' => '0',
						'data-max' => '100',
						'data-type' => 'pixel',
						'default' => '15px',
						'selector' => '.chariti-top-bar-container.chariti-top-bar-full{ padding-right: #gdlr#; padding-left: #gdlr#; }',
						'condition' => array( 'enable-top-bar' => 'enable', 'top-bar-width' => 'full' )
					),
					'top-bar-menu-position' => array(
						'title' => esc_html__('Top Bar Menu Position', 'chariti'),
						'type' => 'combobox',
						'options' => array(
							'none' => esc_html__('None', 'chariti'),
							'left' => esc_html__('Left', 'chariti'),
							'right' => esc_html__('Right', 'chariti'),
						),
						'condition' => array( 'enable-top-bar' => 'enable' )
					),
					'top-bar-left-text' => array(
						'title' => esc_html__('Top Bar Left Text', 'chariti'),
						'type' => 'textarea',
						'condition' => array( 'enable-top-bar' => 'enable' )
					),
					'top-bar-right-text' => array(
						'title' => esc_html__('Top Bar Right Text', 'chariti'),
						'type' => 'textarea',
						'condition' => array( 'enable-top-bar' => 'enable' )
					),
					'top-bar-top-padding' => array(
						'title' => esc_html__('Top Bar Top Padding', 'chariti'),
						'type' => 'fontslider',
						'data-type' => 'pixel',
						'data-min' => '0',
						'data-max' => '200',
 						'default' => '10px',
						'selector' => '.chariti-top-bar{ padding-top: #gdlr#; }',
						'condition' => array( 'enable-top-bar' => 'enable' )
					),
					'top-bar-bottom-padding' => array(
						'title' => esc_html__('Top Bar Bottom Padding', 'chariti'),
						'type' => 'fontslider',
						'data-type' => 'pixel',
						'data-min' => '0',
						'data-max' => '200',
						'default' => '10px',
						'selector' => '.chariti-top-bar{ padding-bottom: #gdlr#; }' .
							'.chariti-top-bar .chariti-top-bar-menu > li > a{ padding-bottom: #gdlr#; }' .  
							'.sf-menu.chariti-top-bar-menu > .chariti-mega-menu .sf-mega, .sf-menu.chariti-top-bar-menu > .chariti-normal-menu ul{ margin-top: #gdlr#; }',
						'condition' => array( 'enable-top-bar' => 'enable' )
					),
					'top-bar-text-size' => array(
						'title' => esc_html__('Top Bar Text Size', 'chariti'),
						'type' => 'fontslider',
						'data-type' => 'pixel',
						'default' => '13px',
						'selector' => '.chariti-top-bar{ font-size: #gdlr#; }',
						'condition' => array( 'enable-top-bar' => 'enable' )
					),
					'top-bar-bottom-border' => array(
						'title' => esc_html__('Top Bar Bottom Border', 'chariti'),
						'type' => 'fontslider',
						'data-type' => 'pixel',
						'data-min' => '0',
						'data-max' => '10',
						'default' => '0',
						'selector' => '.chariti-top-bar{ border-bottom-width: #gdlr#; }',
						'condition' => array( 'enable-top-bar' => 'enable' )
					),
					'top-bar-shadow-size' => array(
						'title' => esc_html__('Top Bar Shadow Size', 'chariti'),
						'type' => 'text',
						'data-input-type' => 'pixel',
						'condition' => array( 'enable-top-bar' => 'enable' )
					),
					'top-bar-shadow-color' => array(
						'title' => esc_html__('Top Bar Shadow Color', 'chariti'),
						'type' => 'colorpicker',
						'data-type' => 'rgba',
						'default' => '#000',
						'selector-extra' => true,
						'selector' => '.chariti-top-bar{ ' . 
							'box-shadow: 0px 0px <top-bar-shadow-size>t rgba(#gdlra#, 0.1); ' . 
							'-webkit-box-shadow: 0px 0px <top-bar-shadow-size>t rgba(#gdlra#, 0.1); ' . 
							'-moz-box-shadow: 0px 0px <top-bar-shadow-size>t rgba(#gdlra#, 0.1); }',
						'condition' => array( 'enable-top-bar' => 'enable' )
					)

				)
			);
		}
	}

	if( !function_exists('chariti_top_bar_social_options') ){
		function chariti_top_bar_social_options(){
			return array(
				'title' => esc_html__('Top Bar Social', 'chariti'),
				'options' => array(
					'enable-top-bar-social' => array(
						'title' => esc_html__('Enable Top Bar Social', 'chariti'),
						'type' => 'checkbox',
						'default' => 'enable'
					),
					'top-bar-social-position' => array(
						'title' => esc_html__('Top Bar Social Position', 'chariti'),
						'type' => 'combobox',
						'options' => array(
							'left' => esc_html__('Left', 'chariti'),
							'right' => esc_html__('Right', 'chariti'),
						),
						'default' => 'right',
						'condition' => array( 'enable-top-bar-social' => 'enable' )
					),
					'top-bar-social-delicious' => array(
						'title' => esc_html__('Top Bar Social Delicious Link', 'chariti'),
						'type' => 'text',
						'condition' => array( 'enable-top-bar-social' => 'enable' )
					),
					'top-bar-social-email' => array(
						'title' => esc_html__('Top Bar Social Email Link', 'chariti'),
						'type' => 'text',
						'condition' => array( 'enable-top-bar-social' => 'enable' )
					),
					'top-bar-social-deviantart' => array(
						'title' => esc_html__('Top Bar Social Deviantart Link', 'chariti'),
						'type' => 'text',
						'condition' => array( 'enable-top-bar-social' => 'enable' )
					),
					'top-bar-social-digg' => array(
						'title' => esc_html__('Top Bar Social Digg Link', 'chariti'),
						'type' => 'text',
						'condition' => array( 'enable-top-bar-social' => 'enable' )
					),
					'top-bar-social-facebook' => array(
						'title' => esc_html__('Top Bar Social Facebook Link', 'chariti'),
						'type' => 'text',
						'condition' => array( 'enable-top-bar-social' => 'enable' )
					),
					'top-bar-social-flickr' => array(
						'title' => esc_html__('Top Bar Social Flickr Link', 'chariti'),
						'type' => 'text',
						'condition' => array( 'enable-top-bar-social' => 'enable' )
					),
					'top-bar-social-google-plus' => array(
						'title' => esc_html__('Top Bar Social Google Plus Link', 'chariti'),
						'type' => 'text',
						'condition' => array( 'enable-top-bar-social' => 'enable' )
					),
					'top-bar-social-lastfm' => array(
						'title' => esc_html__('Top Bar Social Lastfm Link', 'chariti'),
						'type' => 'text',
						'condition' => array( 'enable-top-bar-social' => 'enable' )
					),
					'top-bar-social-linkedin' => array(
						'title' => esc_html__('Top Bar Social Linkedin Link', 'chariti'),
						'type' => 'text',
						'condition' => array( 'enable-top-bar-social' => 'enable' )
					),
					'top-bar-social-pinterest' => array(
						'title' => esc_html__('Top Bar Social Pinterest Link', 'chariti'),
						'type' => 'text',
						'condition' => array( 'enable-top-bar-social' => 'enable' )
					),
					'top-bar-social-rss' => array(
						'title' => esc_html__('Top Bar Social RSS Link', 'chariti'),
						'type' => 'text',
						'condition' => array( 'enable-top-bar-social' => 'enable' )
					),
					'top-bar-social-skype' => array(
						'title' => esc_html__('Top Bar Social Skype Link', 'chariti'),
						'type' => 'text',
						'condition' => array( 'enable-top-bar-social' => 'enable' )
					),
					'top-bar-social-stumbleupon' => array(
						'title' => esc_html__('Top Bar Social Stumbleupon Link', 'chariti'),
						'type' => 'text',
						'condition' => array( 'enable-top-bar-social' => 'enable' )
					),
					'top-bar-social-tumblr' => array(
						'title' => esc_html__('Top Bar Social Tumblr Link', 'chariti'),
						'type' => 'text',
						'condition' => array( 'enable-top-bar-social' => 'enable' )
					),
					'top-bar-social-twitter' => array(
						'title' => esc_html__('Top Bar Social Twitter Link', 'chariti'),
						'type' => 'text',
						'condition' => array( 'enable-top-bar-social' => 'enable' )
					),
					'top-bar-social-vimeo' => array(
						'title' => esc_html__('Top Bar Social Vimeo Link', 'chariti'),
						'type' => 'text',
						'condition' => array( 'enable-top-bar-social' => 'enable' )
					),
					'top-bar-social-youtube' => array(
						'title' => esc_html__('Top Bar Social Youtube Link', 'chariti'),
						'type' => 'text',
						'condition' => array( 'enable-top-bar-social' => 'enable' )
					),
					'top-bar-social-instagram' => array(
						'title' => esc_html__('Top Bar Social Instagram Link', 'chariti'),
						'type' => 'text',
						'condition' => array( 'enable-top-bar-social' => 'enable' )
					),
					'top-bar-social-snapchat' => array(
						'title' => esc_html__('Top Bar Social Snapchat Link', 'chariti'),
						'type' => 'text',
						'condition' => array( 'enable-top-bar-social' => 'enable' )
					),

				)
			);
		}
	}

	if( !function_exists('chariti_header_options') ){
		function chariti_header_options(){
			return array(
				'title' => esc_html__('Header', 'chariti'),
				'options' => array(

					'header-style' => array(
						'title' => esc_html__('Header Style', 'chariti'),
						'type' => 'combobox',
						'options' => array(
							'plain' => esc_html__('Plain', 'chariti'),
							'bar' => esc_html__('Bar', 'chariti'),
							'bar2' => esc_html__('Navigation Boxed', 'chariti'),
							'boxed' => esc_html__('Boxed', 'chariti'),
							'side' => esc_html__('Side Menu', 'chariti'),
							'side-toggle' => esc_html__('Side Menu Toggle', 'chariti'),
						),
						'default' => 'plain',
					),
					'header-plain-style' => array(
						'title' => esc_html__('Header Plain Style', 'chariti'),
						'type' => 'radioimage',
						'options' => array(
							'menu-left' => get_template_directory_uri() . '/images/header/plain-menu-left.jpg',
							'menu-right' => get_template_directory_uri() . '/images/header/plain-menu-right.jpg',
							'center-logo' => get_template_directory_uri() . '/images/header/plain-center-logo.jpg',
							'center-menu' => get_template_directory_uri() . '/images/header/plain-center-menu.jpg',
							'splitted-menu' => get_template_directory_uri() . '/images/header/plain-splitted-menu.jpg',
						),
						'default' => 'menu-right',
						'condition' => array( 'header-style' => 'plain' ),
						'wrapper-class' => 'gdlr-core-fullsize'
					),
					'header-plain-bottom-border' => array(
						'title' => esc_html__('Plain Header Bottom Border', 'chariti'),
						'type' => 'fontslider',
						'data-type' => 'pixel',
						'data-min' => '0',
						'data-max' => '10',
						'default' => '0',
						'selector' => '.chariti-header-style-plain{ border-bottom-width: #gdlr#; }',
						'condition' => array( 'header-style' => array('plain') )
					),
					'header-bar-navigation-align' => array(
						'title' => esc_html__('Header Bar Style', 'chariti'),
						'type' => 'radioimage',
						'options' => array(
							'left' => get_template_directory_uri() . '/images/header/bar-left.jpg',
							'center' => get_template_directory_uri() . '/images/header/bar-center.jpg',
							'center-logo' => get_template_directory_uri() . '/images/header/bar-center-logo.jpg',
						),
						'default' => 'center',
						'condition' => array( 'header-style' => 'bar' ),
						'wrapper-class' => 'gdlr-core-fullsize'
					),
					'header-bar2-navigation-align' => array(
						'title' => esc_html__('Header Bar 2 Style', 'chariti'),
						'type' => 'radioimage',
						'options' => array(
							'left' => get_template_directory_uri() . '/images/header/bar2-left.jpg',
							'center' => get_template_directory_uri() . '/images/header/bar2-center.jpg',
							'center-logo' => get_template_directory_uri() . '/images/header/bar2-center-logo.jpg',
						),
						'default' => 'center',
						'condition' => array( 'header-style' => 'bar2' ),
						'wrapper-class' => 'gdlr-core-fullsize'
					),
					'header-background-style' => array(
						'title' => esc_html__('Header/Navigation Background Style', 'chariti'),
						'type' => 'combobox',
						'options' => array(
							'solid' => esc_html__('Solid', 'chariti'),
							'transparent' => esc_html__('Transparent', 'chariti'),
						),
						'default' => 'solid',
						'condition' => array( 'header-style' => array('plain', 'bar', 'bar2') )
					),
					'top-bar-background-opacity' => array(
						'title' => esc_html__('Top Bar Background Opacity', 'chariti'),
						'type' => 'fontslider',
						'data-type' => 'opacity',
						'default' => '50',
						'condition' => array( 'header-style' => 'plain', 'header-background-style' => 'transparent' ),
						'selector' => '.chariti-header-background-transparent .chariti-top-bar-background{ opacity: #gdlr#; }'
					),
					'header-background-opacity' => array(
						'title' => esc_html__('Header Background Opacity', 'chariti'),
						'type' => 'fontslider',
						'data-type' => 'opacity',
						'default' => '50',
						'condition' => array( 'header-style' => array('plain', 'bar2'), 'header-background-style' => 'transparent' ),
						'selector' => '.chariti-header-background-transparent .chariti-header-background{ opacity: #gdlr#; }'
					),
					'navigation-background-opacity' => array(
						'title' => esc_html__('Navigation Background Opacity', 'chariti'),
						'type' => 'fontslider',
						'data-type' => 'opacity',
						'default' => '50',
						'condition' => array( 'header-style' => array('bar', 'bar2'), 'header-background-style' => 'transparent' ),
						'selector' => '.chariti-navigation-bar-wrap.chariti-style-transparent .chariti-navigation-background{ opacity: #gdlr#; }'
					),
					'header-boxed-style' => array(
						'title' => esc_html__('Header Boxed Style', 'chariti'),
						'type' => 'radioimage',
						'options' => array(
							'menu-right' => get_template_directory_uri() . '/images/header/boxed-menu-right.jpg',
							'center-menu' => get_template_directory_uri() . '/images/header/boxed-center-menu.jpg',
							'splitted-menu' => get_template_directory_uri() . '/images/header/boxed-splitted-menu.jpg',
						),
						'default' => 'menu-right',
						'condition' => array( 'header-style' => 'boxed' ),
						'wrapper-class' => 'gdlr-core-fullsize'
					),
					'boxed-top-bar-background-opacity' => array(
						'title' => esc_html__('Top Bar Background Opacity', 'chariti'),
						'type' => 'fontslider',
						'data-type' => 'opacity',
						'default' => '0',
						'condition' => array( 'header-style' => 'boxed' ),
						'selector' => '.chariti-header-boxed-wrap .chariti-top-bar-background{ opacity: #gdlr#; }'
					),
					'boxed-top-bar-background-extend' => array(
						'title' => esc_html__('Top Bar Background Extend ( Bottom )', 'chariti'),
						'type' => 'fontslider',
						'data-type' => 'pixel',
						'data-min' => '0px',
						'data-max' => '200px',
						'default' => '0px',
						'condition' => array( 'header-style' => 'boxed' ),
						'selector' => '.chariti-header-boxed-wrap .chariti-top-bar-background{ margin-bottom: -#gdlr#; }'
					),
					'boxed-header-top-margin' => array(
						'title' => esc_html__('Header Top Margin', 'chariti'),
						'type' => 'fontslider',
						'data-type' => 'pixel',
						'data-min' => '0px',
						'data-max' => '200px',
						'default' => '0px',
						'condition' => array( 'header-style' => 'boxed' ),
						'selector' => '.chariti-header-style-boxed{ margin-top: #gdlr#; }'
					),
					'header-side-style' => array(
						'title' => esc_html__('Header Side Style', 'chariti'),
						'type' => 'radioimage',
						'options' => array(
							'top-left' => get_template_directory_uri() . '/images/header/side-top-left.jpg',
							'middle-left' => get_template_directory_uri() . '/images/header/side-middle-left.jpg',
							'middle-left-2' => get_template_directory_uri() . '/images/header/side-middle-left-2.jpg',
							'top-right' => get_template_directory_uri() . '/images/header/side-top-right.jpg',
							'middle-right' => get_template_directory_uri() . '/images/header/side-middle-right.jpg',
							'middle-right-2' => get_template_directory_uri() . '/images/header/side-middle-right-2.jpg',
						),
						'default' => 'top-left',
						'condition' => array( 'header-style' => 'side' ),
						'wrapper-class' => 'gdlr-core-fullsize'
					),
					'header-side-align' => array(
						'title' => esc_html__('Header Side Text Align', 'chariti'),
						'type' => 'radioimage',
						'options' => 'text-align',
						'default' => 'left',
						'condition' => array( 'header-style' => 'side' )
					),
					'header-side-toggle-style' => array(
						'title' => esc_html__('Header Side Toggle Style', 'chariti'),
						'type' => 'radioimage',
						'options' => array(
							'left' => get_template_directory_uri() . '/images/header/side-toggle-left.jpg',
							'right' => get_template_directory_uri() . '/images/header/side-toggle-right.jpg',
						),
						'default' => 'left',
						'condition' => array( 'header-style' => 'side-toggle' ),
						'wrapper-class' => 'gdlr-core-fullsize'
					),
					'header-side-toggle-menu-type' => array(
						'title' => esc_html__('Header Side Toggle Menu Type', 'chariti'),
						'type' => 'combobox',
						'options' => array(
							'left' => esc_html__('Left Slide Menu', 'chariti'),
							'right' => esc_html__('Right Slide Menu', 'chariti'),
							'overlay' => esc_html__('Overlay Menu', 'chariti'),
						),
						'default' => 'overlay',
						'condition' => array( 'header-style' => 'side-toggle' )
					),
					'header-side-toggle-display-logo' => array(
						'title' => esc_html__('Display Logo', 'chariti'),
						'type' => 'checkbox',
						'default' => 'enable',
						'condition' => array( 'header-style' => 'side-toggle' )
					),
					'header-width' => array(
						'title' => esc_html__('Header Width', 'chariti'),
						'type' => 'combobox',
						'options' => array(
							'boxed' => esc_html__('Boxed ( Within Container )', 'chariti'),
							'full' => esc_html__('Full', 'chariti'),
							'custom' => esc_html__('Custom', 'chariti'),
						),
						'condition' => array('header-style'=> array('plain', 'bar', 'bar2', 'boxed'))
					),
					'header-width-pixel' => array(
						'title' => esc_html__('Header Width Pixel', 'chariti'),
						'type' => 'text',
						'data-type' => 'pixel',
						'default' => '1140px',
						'condition' => array('header-style'=> array('plain', 'bar', 'bar2', 'boxed'), 'header-width' => 'custom'),
						'selector' => '.chariti-header-container.chariti-header-custom-container{ max-width: #gdlr#; }'
					),
					'header-full-side-padding' => array(
						'title' => esc_html__('Header Full ( Left/Right ) Padding', 'chariti'),
						'type' => 'fontslider',
						'data-min' => '0',
						'data-max' => '100',
						'data-type' => 'pixel',
						'default' => '15px',
						'selector' => '.chariti-header-container.chariti-header-full{ padding-right: #gdlr#; padding-left: #gdlr#; }',
						'condition' => array('header-style'=> array('plain', 'bar', 'bar2', 'boxed'), 'header-width'=>'full')
					),
					'boxed-header-frame-radius' => array(
						'title' => esc_html__('Header Frame Radius', 'chariti'),
						'type' => 'text',
						'data-type' => 'pixel',
						'data-input-type' => 'pixel',
						'default' => '3px',
						'condition' => array( 'header-style' => 'boxed' ),
						'selector' => '.chariti-header-boxed-wrap .chariti-header-background{ border-radius: #gdlr#; -moz-border-radius: #gdlr#; -webkit-border-radius: #gdlr#; }'
					),
					'boxed-header-content-padding' => array(
						'title' => esc_html__('Header Content ( Left/Right ) Padding', 'chariti'),
						'type' => 'fontslider',
						'data-min' => '0',
						'data-max' => '100',
						'data-type' => 'pixel',
						'default' => '30px',
						'selector' => '.chariti-header-style-boxed .chariti-header-container-item{ padding-left: #gdlr#; padding-right: #gdlr#; }' . 
							'.chariti-navigation-right{ right: #gdlr#; } .chariti-navigation-left{ left: #gdlr#; }',
						'condition' => array( 'header-style' => 'boxed' )
					),
					'navigation-text-top-margin' => array(
						'title' => esc_html__('Navigation Text Top Padding', 'chariti'),
						'type' => 'fontslider',
						'data-type' => 'pixel',
						'data-min' => '0',
						'data-max' => '200',
						'default' => '0px',
						'condition' => array( 'header-style' => 'plain', 'header-plain-style' => 'splitted-menu' ),
						'selector' => '.chariti-header-style-plain.chariti-style-splitted-menu .chariti-navigation .sf-menu > li > a{ padding-top: #gdlr#; } ' .
							'.chariti-header-style-plain.chariti-style-splitted-menu .chariti-main-menu-left-wrap,' .
							'.chariti-header-style-plain.chariti-style-splitted-menu .chariti-main-menu-right-wrap{ padding-top: #gdlr#; }'
					),
					'navigation-text-top-margin-boxed' => array(
						'title' => esc_html__('Navigation Text Top Padding', 'chariti'),
						'type' => 'fontslider',
						'data-type' => 'pixel',
						'data-min' => '0',
						'data-max' => '200',
						'default' => '0px',
						'condition' => array( 'header-style' => 'boxed', 'header-boxed-style' => 'splitted-menu' ),
						'selector' => '.chariti-header-style-boxed.chariti-style-splitted-menu .chariti-navigation .sf-menu > li > a{ padding-top: #gdlr#; } ' .
							'.chariti-header-style-boxed.chariti-style-splitted-menu .chariti-main-menu-left-wrap,' .
							'.chariti-header-style-boxed.chariti-style-splitted-menu .chariti-main-menu-right-wrap{ padding-top: #gdlr#; }'
					),
					'navigation-text-side-spacing' => array(
						'title' => esc_html__('Navigation Text Side ( Left / Right ) Spaces', 'chariti'),
						'type' => 'fontslider',
						'data-min' => '0',
						'data-max' => '30',
						'data-type' => 'pixel',
						'default' => '13px',
						'selector' => '.chariti-navigation .sf-menu > li{ padding-left: #gdlr#; padding-right: #gdlr#; }',
						'condition' => array( 'header-style' => array('plain', 'bar', 'bar2', 'boxed') )
					),
					'navigation-left-offset' => array(
						'title' => esc_html__('Navigation Left Offset Spaces', 'chariti'),
						'type' => 'text',
						'data-type' => 'pixel',
						'data-input-type' => 'pixel',
						'default' => '0',
						'selector' => '.chariti-navigation .chariti-main-menu{ margin-left: #gdlr#; }'
					),
					'navigation-slide-bar' => array(
						'title' => esc_html__('Navigation Slide Bar', 'chariti'),
						'type' => 'combobox',
						'options' => array(
							'disable' => esc_html__('Disable', 'chariti'),
							'enable' => esc_html__('Bar With Triangle Style', 'chariti'),
							'style-2' => esc_html__('Bar Style', 'chariti'),
							'style-2-left' => esc_html__('Bar Style Left', 'chariti'),
							'style-dot' => esc_html__('Dot Style', 'chariti')
						),
						'default' => 'enable',
						'condition' => array( 'header-style' => array('plain', 'bar', 'bar2', 'boxed') )
					),
					'navigation-slide-bar-width' => array(
						'title' => esc_html__('Navigation Slide Bar Width', 'chariti'),
						'type' => 'text',
						'data-type' => 'pixel',
						'data-input-type' => 'pixel',
						'condition' => array( 'header-style' => array('plain', 'bar', 'bar2', 'boxed'), 'navigation-slide-bar' => array('style-2', 'style-2-left') )
					),
					'navigation-slide-bar-height' => array(
						'title' => esc_html__('Navigation Slide Bar Height', 'chariti'),
						'type' => 'text',
						'data-type' => 'pixel',
						'data-input-type' => 'pixel',
						'selector' => '.chariti-navigation .chariti-navigation-slide-bar-style-2{ border-bottom-width: #gdlr#; }',
						'condition' => array( 'header-style' => array('plain', 'bar', 'bar2', 'boxed'), 'navigation-slide-bar' => array('style-2', 'style-2-left') )
					),
					'navigation-slide-bar-top-margin' => array(
						'title' => esc_html__('Navigation Slide Bar Top Margin', 'chariti'),
						'type' => 'text',
						'data-type' => 'pixel',
						'data-input-type' => 'pixel',
						'default' => '',
						'selector' => '.chariti-navigation .chariti-navigation-slide-bar{ margin-top: #gdlr#; }',
						'condition' => array( 'header-style' => array('plain', 'bar', 'bar2', 'boxed'), 'navigation-slide-bar' => array('enable', 'style-2', 'style-2-left', 'style-dot') )
					),
					'side-header-width-pixel' => array(
						'title' => esc_html__('Header Width Pixel', 'chariti'),
						'type' => 'fontslider',
						'data-type' => 'pixel',
						'data-min' => '0',
						'data-max' => '600',
						'default' => '340px',
						'condition' => array('header-style' => array('side', 'side-toggle')),
						'selector' => '.chariti-header-side-nav{ width: #gdlr#; }' . 
							'.chariti-header-side-content.chariti-style-left{ margin-left: #gdlr#; }' .
							'.chariti-header-side-content.chariti-style-right{ margin-right: #gdlr#; }'
					),
					'side-header-side-padding' => array(
						'title' => esc_html__('Header Side Padding', 'chariti'),
						'type' => 'fontslider',
						'data-type' => 'pixel',
						'data-min' => '0',
						'data-max' => '200',
						'default' => '70px',
						'condition' => array('header-style' => 'side'),
						'selector' => '.chariti-header-side-nav.chariti-style-side{ padding-left: #gdlr#; padding-right: #gdlr#; }' . 
							'.chariti-header-side-nav.chariti-style-left .sf-vertical > li > ul.sub-menu{ padding-left: #gdlr#; }' .
							'.chariti-header-side-nav.chariti-style-right .sf-vertical > li > ul.sub-menu{ padding-right: #gdlr#; }'
					),
					'navigation-text-top-spacing' => array(
						'title' => esc_html__('Navigation Text Top / Bottom Spaces', 'chariti'),
						'type' => 'fontslider',
						'data-min' => '0',
						'data-max' => '40',
						'data-type' => 'pixel',
						'default' => '16px',
						'selector' => ' .chariti-navigation .sf-vertical > li{ padding-top: #gdlr#; padding-bottom: #gdlr#; }',
						'condition' => array( 'header-style' => array('side') )
					),
					'logo-right-text-top-padding' => array(
						'title' => esc_html__('Header Right Text Top Padding', 'chariti'),
						'type' => 'fontslider',
						'data-type' => 'pixel',
						'data-min' => '0',
						'data-max' => '200',
						'default' => '30px',
						'selector' => '.chariti-header-style-bar .chariti-logo-right-text{ padding-top: #gdlr#; }',
						'condition' => array('header-style' => array('bar', 'bar2'))
					),
					// 'logo-right-text' => array(
					// 	'title' => esc_html__('Header Right Text', 'chariti'),
					// 	'type' => 'textarea',
					// 	'condition' => array('header-style' => array('bar', 'bar2')),
					// ),
					'logo-right-box1-icon' => array(
						'title' => esc_html__('Header Right Box 1 Icon', 'chariti'),
						'type' => 'text',
						'condition' => array('header-style' => array('bar', 'bar2')),
					),
					'logo-right-box1-title' => array(
						'title' => esc_html__('Header Right Box 1 Title', 'chariti'),
						'type' => 'text',
						'condition' => array('header-style' => array('bar', 'bar2')),
					),
					'logo-right-box1-caption' => array(
						'title' => esc_html__('Header Right Box 1 Caption', 'chariti'),
						'type' => 'text',
						'condition' => array('header-style' => array('bar', 'bar2')),
					),
					'logo-right-box2-icon' => array(
						'title' => esc_html__('Header Right Box 2 Icon', 'chariti'),
						'type' => 'text',
						'condition' => array('header-style' => array('bar', 'bar2')),
					),
					'logo-right-box2-title' => array(
						'title' => esc_html__('Header Right Box 2 Title', 'chariti'),
						'type' => 'text',
						'condition' => array('header-style' => array('bar', 'bar2')),
					),
					'logo-right-box2-caption' => array(
						'title' => esc_html__('Header Right Box 2 Caption', 'chariti'),
						'type' => 'text',
						'condition' => array('header-style' => array('bar', 'bar2')),
					),
					'logo-right-box3-icon' => array(
						'title' => esc_html__('Header Right Box 3 Icon', 'chariti'),
						'type' => 'text',
						'condition' => array('header-style' => array('bar', 'bar2')),
					),
					'logo-right-box3-title' => array(
						'title' => esc_html__('Header Right Box 3 Title', 'chariti'),
						'type' => 'text',
						'condition' => array('header-style' => array('bar', 'bar2')),
					),
					'logo-right-box3-caption' => array(
						'title' => esc_html__('Header Right Box 3 Caption', 'chariti'),
						'type' => 'text',
						'condition' => array('header-style' => array('bar', 'bar2')),
					),
					'header-shadow-size' => array(
						'title' => esc_html__('Header Shadow Size', 'chariti'),
						'type' => 'text',
						'data-input-type' => 'pixel',
						'condition' => array( 'header-style' => 'plain' )
					),
					'header-shadow-color' => array(
						'title' => esc_html__('Header Shadow Color', 'chariti'),
						'type' => 'colorpicker',
						'data-type' => 'rgba',
						'default' => '#000',
						'selector-extra' => true,
						'selector' => '.chariti-header-style-plain{ ' . 
							'box-shadow: 0px 0px <header-shadow-size>t rgba(#gdlra#, 0.1); ' . 
							'-webkit-box-shadow: 0px 0px <header-shadow-size>t rgba(#gdlra#, 0.1); ' . 
							'-moz-box-shadow: 0px 0px <header-shadow-size>t rgba(#gdlra#, 0.1); }',
						'condition' => array( 'header-style' => 'plain' )
					)
				)
			);
		}
	}

	if( !function_exists('chariti_logo_options') ){
		function chariti_logo_options(){
			return array(
				'title' => esc_html__('Logo', 'chariti'),
				'options' => array(
					'enable-logo' => array(
						'title' => esc_html__('Enable Logo', 'chariti'),
						'type' => 'checkbox',
						'default' => 'enable'
					),
					'logo' => array(
						'title' => esc_html__('Logo', 'chariti'),
						'type' => 'upload',
						'condition' => array( 'enable-logo' => 'enable' )
					),
					'logo-top-padding' => array(
						'title' => esc_html__('Logo Top Padding', 'chariti'),
						'type' => 'fontslider',
						'data-min' => '0',
						'data-max' => '200',
						'data-type' => 'pixel',
						'default' => '20px',
						'selector' => '.chariti-logo{ padding-top: #gdlr#; }',
						'description' => esc_html__('This option will be omitted on splitted menu option.', 'chariti'),
						'condition' => array( 'enable-logo' => 'enable' )
					),
					'logo-bottom-padding' => array(
						'title' => esc_html__('Logo Bottom Padding', 'chariti'),
						'type' => 'fontslider',
						'data-min' => '0',
						'data-max' => '200',
						'data-type' => 'pixel',
						'default' => '20px',
						'selector' => '.chariti-logo{ padding-bottom: #gdlr#; }',
						'description' => esc_html__('This option will be omitted on splitted menu option.', 'chariti'),
						'condition' => array( 'enable-logo' => 'enable' )
					),
					'logo-left-padding' => array(
						'title' => esc_html__('Logo Left Padding', 'chariti'),
						'type' => 'text',
						'data-type' => 'pixel',
						'data-input-type' => 'pixel',
						'selector' => '.chariti-logo.chariti-item-pdlr{ padding-left: #gdlr#; }',
						'description' => esc_html__('Leave this field blank for default value.', 'chariti'),
						'condition' => array( 'enable-logo' => 'enable' )
					),
					'max-logo-width' => array(
						'title' => esc_html__('Max Logo Width', 'chariti'),
						'type' => 'text',
						'data-type' => 'pixel',
						'data-input-type' => 'pixel',
						'default' => '200px',
						'selector' => '.chariti-logo-inner{ max-width: #gdlr#; }',
						'condition' => array( 'enable-logo' => 'enable' )
					),

					'mobile-logo' => array(
						'title' => esc_html__('Mobile/Tablet Logo', 'chariti'),
						'type' => 'upload',
						'description' => esc_html__('Leave this option blank to use the same logo.', 'chariti'),
						'condition' => array( 'enable-logo' => 'enable' )
					),
					'max-tablet-logo-width' => array(
						'title' => esc_html__('Max Tablet Logo Width', 'chariti'),
						'type' => 'text',
						'data-type' => 'pixel',
						'data-input-type' => 'pixel',
						'selector' => '@media only screen and (max-width: 999px){ .chariti-mobile-header .chariti-logo-inner{ max-width: #gdlr#; } }',
						'condition' => array( 'enable-logo' => 'enable' )
					),
					'max-mobile-logo-width' => array(
						'title' => esc_html__('Max Mobile Logo Width', 'chariti'),
						'type' => 'text',
						'data-type' => 'pixel',
						'data-input-type' => 'pixel',
						'selector' => '@media only screen and (max-width: 767px){ .chariti-mobile-header .chariti-logo-inner{ max-width: #gdlr#; } }',
						'condition' => array( 'enable-logo' => 'enable' )
					),
					'mobile-logo-position' => array(
						'title' => esc_html__('Mobile Logo Position', 'chariti'),
						'type' => 'combobox',
						'options' => array(
							'logo-left' => esc_html__('Logo Left', 'chariti'),
							'logo-center' => esc_html__('Logo Center', 'chariti'),
							'logo-right' => esc_html__('Logo Right', 'chariti'),
						),
						'condition' => array( 'enable-logo' => 'enable' )
					),
				
				)
			);
		}
	}

	if( !function_exists('chariti_navigation_options') ){
		function chariti_navigation_options(){
			return array(
				'title' => esc_html__('Navigation', 'chariti'),
				'options' => array(
					'main-navigation-top-padding' => array(
						'title' => esc_html__('Main Navigation Top Padding', 'chariti'),
						'type' => 'fontslider',
						'data-min' => '0',
						'data-max' => '200',
						'data-type' => 'pixel',
						'default' => '25px',
						'selector' => '.chariti-navigation{ padding-top: #gdlr#; }' . 
							'.chariti-navigation-top{ top: #gdlr#; }'
					),
					'main-navigation-bottom-padding' => array(
						'title' => esc_html__('Main Navigation Bottom Padding', 'chariti'),
						'type' => 'fontslider',
						'data-min' => '0',
						'data-max' => '200',
						'data-type' => 'pixel',
						'default' => '20px',
						'selector' => '.chariti-navigation .sf-menu > li > a{ padding-bottom: #gdlr#; }'
					),
					'main-navigation-item-right-padding' => array(
						'title' => esc_html__('Main Navigation Item Right Padding', 'chariti'),
						'type' => 'fontslider',
						'data-min' => '0',
						'data-max' => '200',
						'data-type' => 'pixel',
						'default' => '0px',
						'selector' => '.chariti-navigation .chariti-main-menu{ padding-right: #gdlr#; }'
					),
					'main-navigation-right-padding' => array(
						'title' => esc_html__('Main Navigation Wrap Right Padding', 'chariti'),
						'type' => 'text',
						'data-type' => 'pixel',
						'data-input-type' => 'pixel',
						'selector' => '.chariti-navigation.chariti-item-pdlr{ padding-right: #gdlr#; }',
						'description' => esc_html__('Leave this field blank for default value.', 'chariti'),
					),
					'enable-main-navigation-submenu-indicator' => array(
						'title' => esc_html__('Enable Main Navigation Submenu Indicator', 'chariti'),
						'type' => 'checkbox',
						'default' => 'disable',
					),
					'navigation-right-top-margin' => array(
						'title' => esc_html__('Navigation Right ( search/cart/button ) Top Margin ', 'chariti'),
						'type' => 'text',
						'data-input-type' => 'pixel',
						'data-type' => 'pixel',
						'selector' => '.chariti-main-menu-right-wrap{ margin-top: #gdlr#; }'
					),
					'enable-main-navigation-search' => array(
						'title' => esc_html__('Enable Main Navigation Search', 'chariti'),
						'type' => 'checkbox',
						'default' => 'enable',
					),
					'main-navigation-search-icon' => array(
						'title' => esc_html__('Main Navigation Search Icon', 'chariti'),
						'type' => 'text',
						'default' => 'fa fa-search',
						'condition' => array('enable-main-navigation-search' => 'enable')
					),
					'main-navigation-search-icon-top-margin' => array(
						'title' => esc_html__('Main Navigation Search Icon Top Margin', 'chariti'),
						'type' => 'text',
						'data-type' => 'pixel',
						'data-input-type' => 'pixel',
						'selector' => '.chariti-main-menu-search{ margin-top: #gdlr#; }',
						'condition' => array('enable-main-navigation-search' => 'enable')
					),
					'enable-main-navigation-cart' => array(
						'title' => esc_html__('Enable Main Navigation Cart ( Woocommerce )', 'chariti'),
						'type' => 'checkbox',
						'default' => 'enable',
						'description' => esc_html__('The icon only shows if the woocommerce plugin is activated', 'chariti')
					),
					'main-navigation-cart-icon' => array(
						'title' => esc_html__('Main Navigation Cart Icon', 'chariti'),
						'type' => 'text',
						'default' => 'fa fa-shopping-cart',
						'condition' => array('enable-main-navigation-search' => 'enable')
					),
					'main-navigation-cart-icon-top-margin' => array(
						'title' => esc_html__('Main Navigation Cart Icon Top Margin', 'chariti'),
						'type' => 'text',
						'data-type' => 'pixel',
						'data-input-type' => 'pixel', 
						'selector' => '.chariti-main-menu-cart{ margin-top: #gdlr#; }',
						'condition' => array('enable-main-navigation-search' => 'enable')
					),
					'enable-main-navigation-right-button' => array(
						'title' => esc_html__('Enable Main Navigation Right Button', 'chariti'),
						'type' => 'checkbox',
						'default' => 'disable',
						'description' => esc_html__('This option will be ignored on header side style', 'chariti')
					),
					'main-navigation-right-button-top-margin' => array(
						'title' => esc_html__('Main Navigation Right Button Top Margin', 'chariti'),
						'type' => 'text',
						'data-input-type' => 'pixel',
						'data-type' => 'pixel',
						'selector' => '.chariti-main-menu-right-button{ margin-top: #gdlr#; }'
					),
					'hide-main-navigation-right-button-in-tablet' => array(
						'title' => esc_html__('Hide Main Navigation Right Button In At 1150px', 'chariti'),
						'type' => 'checkbox',
						'default' => 'disable',
					),
					'main-navigation-right-button-style' => array(
						'title' => esc_html__('Main Navigation Right Button Style', 'chariti'),
						'type' => 'combobox',
						'options' => array(
							'default' => esc_html__('Default', 'chariti'),
							'round' => esc_html__('Round', 'chariti'),
							'round-with-shadow' => esc_html__('Round With Shadow', 'chariti'),
						),
						'condition' => array( 'enable-main-navigation-right-button' => 'enable' ) 
					),
					'main-navigation-right-button-text' => array(
						'title' => esc_html__('Main Navigation Right Button Text', 'chariti'),
						'type' => 'text',
						'default' => esc_html__('Buy Now', 'chariti'),
						'condition' => array( 'enable-main-navigation-right-button' => 'enable' ) 
					),
					'main-navigation-right-button-link' => array(
						'title' => esc_html__('Main Navigation Right Button Link', 'chariti'),
						'type' => 'text',
						'condition' => array( 'enable-main-navigation-right-button' => 'enable' ) 
					),
					'main-navigation-right-button-link-target' => array(
						'title' => esc_html__('Main Navigation Right Button Link Target', 'chariti'),
						'type' => 'combobox',
						'options' => array(
							'_self' => esc_html__('Current Screen', 'chariti'),
							'_blank' => esc_html__('New Window', 'chariti'),
						),
						'condition' => array( 'enable-main-navigation-right-button' => 'enable' ) 
					),
					'main-navigation-right-button-left-margin' => array(
						'title' => esc_html__('Main Navigation Right Button Left Margin', 'chariti'),
						'type' => 'text',
						'data-input-type' => 'pixel',
						'data-type' => 'pixel',
						'selector' => '.chariti-body .chariti-main-menu-right-button.chariti-button-1{ margin-left: #gdlr#; }',
						'condition' => array( 'enable-main-navigation-right-button' => 'enable' ) 
					),
					'main-navigation-right-button-style-2' => array(
						'title' => esc_html__('Main Navigation Right Button Style 2', 'chariti'),
						'type' => 'combobox',
						'options' => array(
							'default' => esc_html__('Default', 'chariti'),
							'round' => esc_html__('Round', 'chariti'),
							'round-with-shadow' => esc_html__('Round With Shadow', 'chariti'),
						),
						'condition' => array( 'enable-main-navigation-right-button' => 'enable' ) 
					),
					'main-navigation-right-button-text-2' => array(
						'title' => esc_html__('Main Navigation Right Button Text 2', 'chariti'),
						'type' => 'text',
						'default' => esc_html__('Buy Now', 'chariti'),
						'condition' => array( 'enable-main-navigation-right-button' => 'enable' ) 
					),
					'main-navigation-right-button-link-2' => array(
						'title' => esc_html__('Main Navigation Right Button Link 2', 'chariti'),
						'type' => 'text',
						'condition' => array( 'enable-main-navigation-right-button' => 'enable' ) 
					),
					'main-navigation-right-button-link-target-2' => array(
						'title' => esc_html__('Main Navigation Right Button Link Target 2', 'chariti'),
						'type' => 'combobox',
						'options' => array(
							'_self' => esc_html__('Current Screen', 'chariti'),
							'_blank' => esc_html__('New Window', 'chariti'),
						),
						'condition' => array( 'enable-main-navigation-right-button' => 'enable' ) 
					),
					'main-navigation-right-button2-left-margin' => array(
						'title' => esc_html__('Main Navigation Right Button 2 Left Margin', 'chariti'),
						'type' => 'text',
						'data-input-type' => 'pixel',
						'data-type' => 'pixel',
						'selector' => '.chariti-body .chariti-main-menu-right-button.chariti-button-2{ margin-left: #gdlr#; }',
						'condition' => array( 'enable-main-navigation-right-button' => 'enable' ) 
					),
					'right-menu-type' => array(
						'title' => esc_html__('Secondary/Mobile Menu Type', 'chariti'),
						'type' => 'combobox',
						'options' => array(
							'left' => esc_html__('Left Slide Menu', 'chariti'),
							'right' => esc_html__('Right Slide Menu', 'chariti'),
							'overlay' => esc_html__('Overlay Menu', 'chariti'),
						),
						'default' => 'right'
					),
					'right-menu-style' => array(
						'title' => esc_html__('Secondary/Mobile Menu Style', 'chariti'),
						'type' => 'combobox',
						'options' => array(
							'hamburger-with-border' => esc_html__('Hamburger With Border ( Font Awesome )', 'chariti'),
							'hamburger' => esc_html__('Hamburger', 'chariti'),
							'hamburger-small' => esc_html__('Hamburger Small', 'chariti'),
						),
						'default' => 'hamburger-with-border'
					),
					
				) // logo-options
			);
		}
	}

	if( !function_exists('chariti_fixed_navigation_options') ){
		function chariti_fixed_navigation_options(){
			return array(
				'title' => esc_html__('Fixed Navigation', 'chariti'),
				'options' => array(

					'enable-main-navigation-sticky' => array(
						'title' => esc_html__('Enable Fixed Navigation Bar', 'chariti'),
						'type' => 'checkbox',
						'default' => 'enable',
					),
					'enable-logo-on-main-navigation-sticky' => array(
						'title' => esc_html__('Enable Logo on Fixed Navigation Bar', 'chariti'),
						'type' => 'checkbox',
						'default' => 'enable',
						'description' => esc_html__('This option will be omitted when the logo is disabeld', 'chariti'),
						'condition' => array( 'enable-main-navigation-sticky' => 'enable' )
					),
					'fixed-navigation-bar-logo' => array(
						'title' => esc_html__('Fixed Navigation Bar Logo', 'chariti'),
						'type' => 'upload',
						'description' => esc_html__('Leave blank to show default logo', 'chariti'),
						'condition' => array( 'enable-main-navigation-sticky' => 'enable', 'enable-logo-on-main-navigation-sticky' => 'enable' )
					),
					'fixed-navigation-max-logo-width' => array(
						'title' => esc_html__('Fixed Navigation Max Logo Width', 'chariti'),
						'type' => 'text',
						'data-type' => 'pixel',
						'data-input-type' => 'pixel',
						'default' => '',
						'condition' => array( 'enable-main-navigation-sticky' => 'enable' ),
						'selector' => '.chariti-fixed-navigation.chariti-style-slide .chariti-logo-inner img{ max-height: none !important; }' .
							'.chariti-animate-fixed-navigation.chariti-header-style-plain .chariti-logo-inner, ' . 
							'.chariti-animate-fixed-navigation.chariti-header-style-boxed .chariti-logo-inner{ max-width: #gdlr#; }' . 
							'.chariti-mobile-header.chariti-fixed-navigation .chariti-logo-inner{ max-width: #gdlr#; }'
					),
					'fixed-navigation-logo-top-padding' => array(
						'title' => esc_html__('Fixed Navigation Logo Top Padding', 'chariti'),
						'type' => 'text',
						'data-type' => 'pixel',
						'data-input-type' => 'pixel',
						'default' => '20px',
						'condition' => array( 'enable-main-navigation-sticky' => 'enable' ),
						'selector' => '.chariti-animate-fixed-navigation.chariti-header-style-plain .chariti-logo, ' . 
							'.chariti-animate-fixed-navigation.chariti-header-style-boxed .chariti-logo{ padding-top: #gdlr#; }'
					),
					'fixed-navigation-logo-bottom-padding' => array(
						'title' => esc_html__('Fixed Navigation Logo Bottom Padding', 'chariti'),
						'type' => 'text',
						'data-type' => 'pixel',
						'data-input-type' => 'pixel',
						'default' => '20px',
						'condition' => array( 'enable-main-navigation-sticky' => 'enable' ),
						'selector' => '.chariti-animate-fixed-navigation.chariti-header-style-plain .chariti-logo, ' . 
							'.chariti-animate-fixed-navigation.chariti-header-style-boxed .chariti-logo{ padding-bottom: #gdlr#; }'
					),
					'fixed-navigation-top-padding' => array(
						'title' => esc_html__('Fixed Navigation Top Padding', 'chariti'),
						'type' => 'text',
						'data-type' => 'pixel',
						'data-input-type' => 'pixel',
						'default' => '30px',
						'condition' => array( 'enable-main-navigation-sticky' => 'enable' ),
						'selector' => '.chariti-animate-fixed-navigation.chariti-header-style-plain .chariti-navigation, ' . 
							'.chariti-animate-fixed-navigation.chariti-header-style-boxed .chariti-navigation{ padding-top: #gdlr#; }' . 
							'.chariti-animate-fixed-navigation.chariti-header-style-plain .chariti-navigation-top, ' . 
							'.chariti-animate-fixed-navigation.chariti-header-style-boxed .chariti-navigation-top{ top: #gdlr#; }' .
							'.chariti-animate-fixed-navigation.chariti-navigation-bar-wrap .chariti-navigation{ padding-top: #gdlr#; }'
					),
					'fixed-navigation-bottom-padding' => array(
						'title' => esc_html__('Fixed Navigation Bottom Padding', 'chariti'),
						'type' => 'text',
						'data-type' => 'pixel',
						'data-input-type' => 'pixel',
						'default' => '25px',
						'condition' => array( 'enable-main-navigation-sticky' => 'enable' ),
						'selector' => '.chariti-animate-fixed-navigation.chariti-header-style-plain .chariti-navigation .sf-menu > li > a, ' . 
							'.chariti-animate-fixed-navigation.chariti-header-style-boxed .chariti-navigation .sf-menu > li > a{ padding-bottom: #gdlr#; }' .
							'.chariti-animate-fixed-navigation.chariti-navigation-bar-wrap .chariti-navigation .sf-menu > li > a{ padding-bottom: #gdlr#; }' .
							'.chariti-animate-fixed-navigation .chariti-main-menu-right{ margin-bottom: #gdlr#; }'
					),
					'enable-fixed-navigation-slide-bar' => array(
						'title' => esc_html__('Enable Fixed Navigation Slide Bar', 'chariti'),
						'type' => 'checkbox',
						'default' => 'enable'
					),
					'fixed-navigation-slide-bar-top-margin' => array(
						'title' => esc_html__('Fixed Navigation Slide Bar Top Margin', 'chariti'),
						'type' => 'text',
						'data-type' => 'pixel',
						'data-input-type' => 'pixel',
						'default' => '',
						'selector' => '.chariti-fixed-navigation .chariti-navigation .chariti-navigation-slide-bar{ margin-top: #gdlr#; }',
						'condition' => array('enable-fixed-navigation-slide-bar' => 'enable')
					),
					'fixed-navigation-anchor-offset' => array(
						'title' => esc_html__('Fixed Navigation Anchor Offset ( Fixed Navigation Height )', 'chariti'),
						'type' => 'text',
						'data-type' => 'pixel',
						'data-input-type' => 'pixel',
						'default' => '75px',
						'condition' => array( 'enable-main-navigation-sticky' => 'enable' ),
					),
					'enable-mobile-navigation-sticky' => array(
						'title' => esc_html__('Enable Mobile Fixed Navigation Bar', 'chariti'),
						'type' => 'checkbox',
						'default' => 'enable',
					),

				)
			);
		}
	}

	if( !function_exists('chariti_header_color_options') ){
		function chariti_header_color_options(){

			return array(
				'title' => esc_html__('Header', 'chariti'),
				'options' => array(
					'top-bar-background-color' => array(
						'title' => esc_html__('Top Bar Background Color', 'chariti'),
						'type' => 'colorpicker',
						'default' => '#222222',
						'selector' => '.chariti-top-bar-background{ background-color: #gdlr#; }'
					),
					'top-bar-bottom-border-color' => array(
						'title' => esc_html__('Top Bar Bottom Border Color', 'chariti'),
						'type' => 'colorpicker',
						'default' => '#ffffff',
						'selector' => '.chariti-body .chariti-top-bar{ border-bottom-color: #gdlr#; }'
					),
					'top-bar-text-color' => array(
						'title' => esc_html__('Top Bar Text Color', 'chariti'),
						'type' => 'colorpicker',
						'default' => '#ffffff',
						'selector' => '.chariti-top-bar{ color: #gdlr#; }'
					),
					'top-bar-link-color' => array(
						'title' => esc_html__('Top Bar Link Color', 'chariti'),
						'type' => 'colorpicker',
						'default' => '#ffffff',
						'selector' => '.chariti-body .chariti-top-bar a{ color: #gdlr#; }'
					),
					'top-bar-link-hover-color' => array(
						'title' => esc_html__('Top Bar Link Hover Color', 'chariti'),
						'type' => 'colorpicker',
						'default' => '#ffffff',
						'selector' => '.chariti-body .chariti-top-bar a:hover{ color: #gdlr#; }'
					),
					'top-bar-social-color' => array(
						'title' => esc_html__('Top Bar Social Color', 'chariti'),
						'type' => 'colorpicker',
						'default' => '#ffffff',
						'selector' => '.chariti-top-bar .chariti-top-bar-right-social a{ color: #gdlr#; }'
					),
					'top-bar-social-hover-color' => array(
						'title' => esc_html__('Top Bar Social Hover Color', 'chariti'),
						'type' => 'colorpicker',
						'default' => '#e44444',
						'selector' => '.chariti-top-bar .chariti-top-bar-right-social a:hover{ color: #gdlr#; }'
					),
					'header-background-color' => array(
						'title' => esc_html__('Header Background Color', 'chariti'),
						'type' => 'colorpicker',
						'default' => '#ffffff',
						'selector' => '.chariti-header-background, .chariti-sticky-menu-placeholder, .chariti-header-style-boxed.chariti-fixed-navigation, ' . 
							'body.single-product .chariti-header-background-transparent, body.single-cause .chariti-header-background-transparent{ background-color: #gdlr#; }'
					),
					'header-plain-bottom-border-color' => array(
						'title' => esc_html__('Header Bottom Border Color ( Header Plain Style )', 'chariti'),
						'type' => 'colorpicker',
						'default' => '#e8e8e8',
						'selector' => '.chariti-header-wrap.chariti-header-style-plain{ border-color: #gdlr#; }'
					),
					'logo-background-color' => array(
						'title' => esc_html__('Logo Background Color ( Header Side Menu Toggle Style )', 'chariti'),
						'type' => 'colorpicker',
						'default' => '#ffffff',
						'selector' => '.chariti-header-side-nav.chariti-style-side-toggle .chariti-logo{ background-color: #gdlr#; }'
					),
					'secondary-menu-icon-color' => array(
						'title' => esc_html__('Secondary Menu Icon Color', 'chariti'),
						'type' => 'colorpicker',
						'default' => '#383838',
						'selector'=> '.chariti-top-menu-button i, .chariti-mobile-menu-button i{ color: #gdlr#; }' . 
							'.chariti-mobile-button-hamburger:before, ' . 
							'.chariti-mobile-button-hamburger:after, ' . 
							'.chariti-mobile-button-hamburger span, ' . 
							'.chariti-mobile-button-hamburger-small:before, ' . 
							'.chariti-mobile-button-hamburger-small:after, ' . 
							'.chariti-mobile-button-hamburger-small span{ background: #gdlr#; }'
					),
					'secondary-menu-border-color' => array(
						'title' => esc_html__('Secondary Menu Border Color', 'chariti'),
						'type' => 'colorpicker',
						'default' => '#dddddd',
						'selector'=> '.chariti-main-menu-right .chariti-top-menu-button, .chariti-mobile-menu .chariti-mobile-menu-button{ border-color: #gdlr#; }'
					),
					'search-overlay-background-color' => array(
						'title' => esc_html__('Search Overlay Background Color', 'chariti'),
						'type' => 'colorpicker',
						'data-type' => 'rgba',
						'default' => '#000000',
						'selector'=> '.chariti-top-search-wrap{ background-color: #gdlr#; background-color: rgba(#gdlra#, 0.88); }'
					),
					'top-cart-background-color' => array(
						'title' => esc_html__('Top Cart Background Color', 'chariti'),
						'type' => 'colorpicker',
						'default' => '#ffffff',
						'selector'=> '.chariti-top-cart-content-wrap .chariti-top-cart-content{ background-color: #gdlr#; }'
					),
					'top-cart-title-color' => array(
						'title' => esc_html__('Top Cart Title Color', 'chariti'),
						'type' => 'colorpicker',
						'default' => '#000000',
						'selector'=> '.chariti-top-cart-content-wrap .chariti-top-cart-title, .chariti-top-cart-item .chariti-top-cart-item-title, ' . 
							'.chariti-top-cart-item .chariti-top-cart-item-remove{ color: #gdlr#; }'
					),
					'top-cart-info-color' => array(
						'title' => esc_html__('Top Cart Info Color', 'chariti'),
						'type' => 'colorpicker',
						'default' => '#6c6c6c',
						'selector'=> '.chariti-top-cart-content-wrap .woocommerce-Price-amount.amount{ color: #gdlr#; }'
					),
					'top-cart-view-cart-color' => array(
						'title' => esc_html__('Top Cart : View Cart Text Color', 'chariti'),
						'type' => 'colorpicker',
						'default' => '#323232',
						'selector'=> '.chariti-body .chariti-top-cart-button-wrap .chariti-top-cart-button{ color: #gdlr#; }'
					),
					'top-cart-view-cart-background-color' => array(
						'title' => esc_html__('Top Cart : View Cart Background Color', 'chariti'),
						'type' => 'colorpicker',
						'default' => '#f4f4f4',
						'selector'=> '.chariti-body .chariti-top-cart-button-wrap .chariti-top-cart-button{ background-color: #gdlr#; }'
					),
					'top-cart-checkout-color' => array(
						'title' => esc_html__('Top Cart : Checkout Text Color', 'chariti'),
						'type' => 'colorpicker',
						'default' => '#ffffff',
						'selector'=> '.chariti-body .chariti-top-cart-button-wrap .chariti-top-cart-button-2{ color: #gdlr#; }'
					),
					'top-cart-checkout-background-color' => array(
						'title' => esc_html__('Top Cart : Checkout Background Color', 'chariti'),
						'type' => 'colorpicker',
						'default' => '#000000',
						'selector'=> '.chariti-body .chariti-top-cart-button-wrap .chariti-top-cart-button-2{ background-color: #gdlr#; }'
					),
					'breadcrumbs-text-color' => array(
						'title' => esc_html__('Breadcrumbs ( Plugin ) Text Color', 'chariti'),
						'type' => 'colorpicker',
						'data-type' => 'rgba',
						'default' => '#c0c0c0',
						'selector'=> '.chariti-body .chariti-breadcrumbs, .chariti-body .chariti-breadcrumbs a span, ' . 
							'.gdlr-core-breadcrumbs-item, .gdlr-core-breadcrumbs-item a span{ color: #gdlr#; }'
					),
					'breadcrumbs-text-active-color' => array(
						'title' => esc_html__('Breadcrumbs ( Plugin ) Text Active Color', 'chariti'),
						'type' => 'colorpicker',
						'data-type' => 'rgba',
						'default' => '#777777',
						'selector'=> '.chariti-body .chariti-breadcrumbs span, .chariti-body .chariti-breadcrumbs a:hover span, ' . 
							'.gdlr-core-breadcrumbs-item span, .gdlr-core-breadcrumbs-item a:hover span{ color: #gdlr#; }'
					),
				) // header-options
			);

		}
	}

	if( !function_exists('chariti_navigation_color_options') ){
		function chariti_navigation_color_options(){

			return array(
				'title' => esc_html__('Menu', 'chariti'),
				'options' => array(

					'navigation-bar-background-color' => array(
						'title' => esc_html__('Navigation Bar Background Color ( Header Bar Style )', 'chariti'),
						'type' => 'colorpicker',
						'default' => '#f4f4f4',
						'selector' => '.chariti-navigation-background{ background-color: #gdlr#; }'
					),
					'navigation-bar-top-border-color' => array(
						'title' => esc_html__('Navigation Bar Top Border Color ( Header Bar Style )', 'chariti'),
						'type' => 'colorpicker',
						'default' => '#e8e8e8',
						'selector' => '.chariti-navigation-bar-wrap{ border-color: #gdlr#; }'
					),
					'header-right-box-icon-color' => array(
						'title' => esc_html__('Header Right Box Icon Color ( Header Bar Style )', 'chariti'),
						'type' => 'colorpicker',
						'default' => '#f3f3f3',
						'selector' => '.chariti-logo-right-text .chariti-logo-right-text-icon{ color: #gdlr#; }'
					),
					'header-right-box-title-color' => array(
						'title' => esc_html__('Header Right Box Title Color ( Header Bar Style )', 'chariti'),
						'type' => 'colorpicker',
						'default' => '#f3f3f3',
						'selector' => '.chariti-logo-right-text .chariti-logo-right-text-title{ color: #gdlr#; }'
					),
					'header-right-caption-color' => array(
						'title' => esc_html__('Header Right Box Caption Color ( Header Bar Style )', 'chariti'),
						'type' => 'colorpicker',
						'default' => '#f3f3f3',
						'selector' => '.chariti-logo-right-text .chariti-logo-right-text-caption{ color: #gdlr#; }'
					),
					'navigation-slide-bar-color' => array(
						'title' => esc_html__('Navigation Slide Bar Color', 'chariti'),
						'type' => 'colorpicker',
						'default' => '#2d9bea',
						'selector' => '.chariti-navigation .chariti-navigation-slide-bar, ' . 
							'.chariti-navigation .chariti-navigation-slide-bar-style-dot:before{ border-color: #gdlr#; }' . 
							'.chariti-navigation .chariti-navigation-slide-bar:before{ border-bottom-color: #gdlr#; }'
					),
					'main-menu-text-color' => array(
						'title' => esc_html__('Main Menu Text Color', 'chariti'),
						'type' => 'colorpicker',
						'default' => '#999999',
						'selector' => '.sf-menu > li > a, .sf-vertical > li > a{ color: #gdlr#; }'
					),
					'main-menu-text-hover-color' => array(
						'title' => esc_html__('Main Menu Text Hover Color', 'chariti'),
						'type' => 'colorpicker',
						'default' => '#333333',
						'selector' => '.sf-menu > li > a:hover, ' . 
							'.sf-menu > li.current-menu-item > a, ' .
							'.sf-menu > li.current-menu-ancestor > a, ' .
							'.sf-vertical > li > a:hover, ' . 
							'.sf-vertical > li.current-menu-item > a, ' .
							'.sf-vertical > li.current-menu-ancestor > a{ color: #gdlr#; }'
					),
					'sub-menu-background-color' => array(
						'title' => esc_html__('Sub Menu Background Color', 'chariti'),
						'type' => 'colorpicker',
						'default' => '#2e2e2e',
						'selector'=> '.sf-menu > .chariti-normal-menu li, .sf-menu > .chariti-mega-menu > .sf-mega, ' . 
							'.sf-vertical ul.sub-menu li, ul.sf-menu > .menu-item-language li{ background-color: #gdlr#; }'
					),
					'sub-menu-text-color' => array(
						'title' => esc_html__('Sub Menu Text Color', 'chariti'),
						'type' => 'colorpicker',
						'default' => '#bebebe',
						'selector'=> '.sf-menu > li > .sub-menu a, .sf-menu > .chariti-mega-menu > .sf-mega a, ' . 
							'.sf-vertical ul.sub-menu li a{ color: #gdlr#; }'
					),
					'sub-menu-text-hover-color' => array(
						'title' => esc_html__('Sub Menu Text Hover Color', 'chariti'),
						'type' => 'colorpicker',
						'default' => '#ffffff',
						'selector'=> '.sf-menu > li > .sub-menu a:hover, ' . 
							'.sf-menu > li > .sub-menu .current-menu-item > a, ' . 
							'.sf-menu > li > .sub-menu .current-menu-ancestor > a, '.
							'.sf-menu > .chariti-mega-menu > .sf-mega a:hover, '.
							'.sf-menu > .chariti-mega-menu > .sf-mega .current-menu-item > a, '.
							'.sf-vertical > li > .sub-menu a:hover, ' . 
							'.sf-vertical > li > .sub-menu .current-menu-item > a, ' . 
							'.sf-vertical > li > .sub-menu .current-menu-ancestor > a{ color: #gdlr#; }'
					),
					'sub-menu-text-hover-background-color' => array(
						'title' => esc_html__('Sub Menu Text Hover Background', 'chariti'),
						'type' => 'colorpicker',
						'default' => '#393939',
						'selector'=> '.sf-menu > li > .sub-menu a:hover, ' . 
							'.sf-menu > li > .sub-menu .current-menu-item > a, ' . 
							'.sf-menu > li > .sub-menu .current-menu-ancestor > a, '.
							'.sf-menu > .chariti-mega-menu > .sf-mega a:hover, '.
							'.sf-menu > .chariti-mega-menu > .sf-mega .current-menu-item > a, '.
							'.sf-vertical > li > .sub-menu a:hover, ' . 
							'.sf-vertical > li > .sub-menu .current-menu-item > a, ' . 
							'.sf-vertical > li > .sub-menu .current-menu-ancestor > a{ background-color: #gdlr#; }'
					),
					'sub-mega-menu-title-color' => array(
						'title' => esc_html__('Sub Mega Menu Title Color', 'chariti'),
						'type' => 'colorpicker',
						'default' => '#ffffff',
						'selector'=> '.chariti-navigation .sf-menu > .chariti-mega-menu .sf-mega-section-inner > a{ color: #gdlr#; }'
					),
					'sub-mega-menu-divider-color' => array(
						'title' => esc_html__('Sub Mega Menu Divider Color', 'chariti'),
						'type' => 'colorpicker',
						'default' => '#424242',
						'selector'=> '.chariti-navigation .sf-menu > .chariti-mega-menu .sf-mega-section{ border-color: #gdlr#; }'
					),
					'side-menu-text-color' => array(
						'title' => esc_html__('Side Menu Text Color', 'chariti'),
						'type' => 'colorpicker',
						'default' => '#979797',
						'selector'=> '.mm-navbar .mm-title, .mm-navbar .mm-btn, ul.mm-listview li > a, ul.mm-listview li > span{ color: #gdlr#; }' . 
							'ul.mm-listview li a{ border-color: #gdlr#; }' .
							'.mm-arrow:after, .mm-next:after, .mm-prev:before{ border-color: #gdlr#; }'
					),
					'side-menu-text-hover-color' => array(
						'title' => esc_html__('Side Menu Text Hover Color', 'chariti'),
						'type' => 'colorpicker',
						'default' => '#ffffff',
						'selector'=> '.mm-navbar .mm-title:hover, .mm-navbar .mm-btn:hover, ' .
							'ul.mm-listview li a:hover, ul.mm-listview li > span:hover, ' . 
							'ul.mm-listview li.current-menu-item > a, ul.mm-listview li.current-menu-ancestor > a, ul.mm-listview li.current-menu-ancestor > span{ color: #gdlr#; }'
					),
					'side-menu-background-color' => array(
						'title' => esc_html__('Side Menu Background Color', 'chariti'),
						'type' => 'colorpicker',
						'default' => '#1f1f1f',
						'selector'=> '.mm-menu{ background-color: #gdlr#; }'
					),
					'side-menu-border-color' => array(
						'title' => esc_html__('Side Menu Border Color', 'chariti'),
						'type' => 'colorpicker',
						'default' => '#626262',
						'selector'=> 'ul.mm-listview li{ border-color: #gdlr#; }'
					),
					'overlay-menu-background-color' => array(
						'title' => esc_html__('Overlay Menu Background Color', 'chariti'),
						'type' => 'colorpicker',
						'data-type' => 'rgba',
						'default' => '#000000',
						'selector'=> '.chariti-overlay-menu-content{ background-color: #gdlr#; background-color: rgba(#gdlra#, 0.88); }'
					),
					'overlay-menu-border-color' => array(
						'title' => esc_html__('Overlay Menu Border Color', 'chariti'),
						'type' => 'colorpicker',
						'default' => '#424242',
						'selector'=> '.chariti-overlay-menu-content ul.menu > li, .chariti-overlay-menu-content ul.sub-menu ul.sub-menu{ border-color: #gdlr#; }'
					),
					'overlay-menu-text-color' => array(
						'title' => esc_html__('Overlay Menu Text Color', 'chariti'),
						'type' => 'colorpicker',
						'default' => '#ffffff',
						'selector'=> '.chariti-overlay-menu-content ul li a, .chariti-overlay-menu-content .chariti-overlay-menu-close{ color: #gdlr#; }'
					),
					'overlay-menu-text-hover-color' => array(
						'title' => esc_html__('Overlay Menu Text Hover Color', 'chariti'),
						'type' => 'colorpicker',
						'default' => '#a8a8a8',
						'selector'=> '.chariti-overlay-menu-content ul li a:hover{ color: #gdlr#; }'
					),
					'anchor-bullet-background-color' => array(
						'title' => esc_html__('Anchor Bullet Background', 'chariti'),
						'type' => 'colorpicker',
						'default' => '#777777',
						'selector'=> '.chariti-bullet-anchor a:before{ background-color: #gdlr#; }'
					),
					'anchor-bullet-background-active-color' => array(
						'title' => esc_html__('Anchor Bullet Background Active', 'chariti'),
						'type' => 'colorpicker',
						'default' => '#ffffff',
						'selector'=> '.chariti-bullet-anchor a:hover, .chariti-bullet-anchor a.current-menu-item{ border-color: #gdlr#; }' .
							'.chariti-bullet-anchor a:hover:before, .chariti-bullet-anchor a.current-menu-item:before{ background: #gdlr#; }'
					),		
				) // navigation-menu-options
			);	

		}
	}

	if( !function_exists('chariti_navigation_right_color_options') ){
		function chariti_navigation_right_color_options(){

			return array(
				'title' => esc_html__('Navigation Right', 'chariti'),
				'options' => array(

					'navigation-bar-right-icon-color' => array(
						'title' => esc_html__('Navigation Bar Right Icon Color', 'chariti'),
						'type' => 'colorpicker',
						'default' => '#383838',
						'selector'=> '.chariti-main-menu-search i, .chariti-main-menu-cart i{ color: #gdlr#; }'
					),
					'woocommerce-cart-icon-number-background' => array(
						'title' => esc_html__('Woocommmerce Cart\'s Icon Number Background', 'chariti'),
						'type' => 'colorpicker',
						'default' => '#bd584e',
						'selector'=> '.chariti-main-menu-cart > .chariti-top-cart-count{ background-color: #gdlr#; }'
					),
					'woocommerce-cart-icon-number-color' => array(
						'title' => esc_html__('Woocommmerce Cart\'s Icon Number Color', 'chariti'),
						'type' => 'colorpicker',
						'default' => '#ffffff',
						'selector'=> '.chariti-main-menu-cart > .chariti-top-cart-count{ color: #gdlr#; }'
					),
					'navigation-right-button-text-color' => array(
						'title' => esc_html__('Navigation Right Button Text Color', 'chariti'),
						'type' => 'colorpicker',
						'default' => '#333333',
						'selector'=> '.chariti-body .chariti-main-menu-right-button{ color: #gdlr#; }'
					),
					'navigation-right-button-text-hover-color' => array(
						'title' => esc_html__('Navigation Right Button Text Hover Color', 'chariti'),
						'type' => 'colorpicker',
						'default' => '#555555',
						'selector'=> '.chariti-body .chariti-main-menu-right-button:hover{ color: #gdlr#; }'
					),
					'navigation-right-button-background-color' => array(
						'title' => esc_html__('Navigation Right Button Background Color', 'chariti'),
						'type' => 'colorpicker',
						'default' => '',
						'selector'=> '.chariti-body .chariti-main-menu-right-button{ background-color: #gdlr#; }'
					),
					'navigation-right-button-background-hover-color' => array(
						'title' => esc_html__('Navigation Right Button Background Hover Color', 'chariti'),
						'type' => 'colorpicker',
						'default' => '',
						'selector'=> '.chariti-body .chariti-main-menu-right-button:hover{ background-color: #gdlr#; }'
					),
					'navigation-right-button-border-color' => array(
						'title' => esc_html__('Navigation Right Button Border Color', 'chariti'),
						'type' => 'colorpicker',
						'default' => '#333333',
						'selector'=> '.chariti-body .chariti-main-menu-right-button{ border-color: #gdlr#; }'
					),
					'navigation-right-button-border-hover-color' => array(
						'title' => esc_html__('Navigation Right Button Border Hover Color', 'chariti'),
						'type' => 'colorpicker',
						'default' => '#555555',
						'selector'=> '.chariti-body .chariti-main-menu-right-button:hover{ border-color: #gdlr#; }'
					),
					'navigation-right-button2-text-color' => array(
						'title' => esc_html__('Navigation Right Button 2 Text Color', 'chariti'),
						'type' => 'colorpicker',
						'selector'=> '.chariti-body .chariti-main-menu-right-button.chariti-button-2{ color: #gdlr#; }'
					),
					'navigation-right-button2-text-hover-color' => array(
						'title' => esc_html__('Navigation Right Button 2 Text Hover Color', 'chariti'),
						'type' => 'colorpicker',
						'selector'=> '.chariti-body .chariti-main-menu-right-button.chariti-button-2:hover{ color: #gdlr#; }'
					),
					'navigation-right-button2-background-color' => array(
						'title' => esc_html__('Navigation Right Button 2 Background Color', 'chariti'),
						'type' => 'colorpicker',
						'selector'=> '.chariti-body .chariti-main-menu-right-button.chariti-button-2{ background-color: #gdlr#; }'
					),
					'navigation-right-button2-background-hover-color' => array(
						'title' => esc_html__('Navigation Right Button 2 Background Hover Color', 'chariti'),
						'type' => 'colorpicker',
						'selector'=> '.chariti-body .chariti-main-menu-right-button.chariti-button-2:hover{ background-color: #gdlr#; }'
					),
					'navigation-right-button2-border-color' => array(
						'title' => esc_html__('Navigation Right Button 2 Border Color', 'chariti'),
						'type' => 'colorpicker',
						'selector'=> '.chariti-body .chariti-main-menu-right-button.chariti-button-2{ border-color: #gdlr#; }'
					),
					'navigation-right-button2-border-hover-color' => array(
						'title' => esc_html__('Navigation Right Button 2 Border Hover Color', 'chariti'),
						'type' => 'colorpicker',
						'selector'=> '.chariti-body .chariti-main-menu-right-button.chariti-button-2:hover{ border-color: #gdlr#; }'
					),
					'navigation-right-button-shadow-color' => array(
						'title' => esc_html__('Main Navigation Right Button Shadow Color', 'chariti'),
						'type' => 'colorpicker',
						'data-type' => 'rgba',
						'default' => '#000',
						'selector' => '.chariti-main-menu-right-button.chariti-style-round-with-shadow{ box-shadow: 0px 4px 18px rgba(#gdlra#, 0.11); -webkit-box-shadow: 0px 4px 18px rgba(#gdlra#, 0.11); } '
					),

				)
			);

		}
	}