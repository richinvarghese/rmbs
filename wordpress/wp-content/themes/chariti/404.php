<?php
/**
 * The template for displaying 404 pages (not found)
 */

	get_header();
	
	?>
	<div class="chariti-not-found-wrap" id="chariti-full-no-header-wrap" >
		<div class="chariti-not-found-background" ></div>
		<div class="chariti-not-found-container chariti-container">
			<div class="chariti-header-transparent-substitute" ></div>
	
			<div class="chariti-not-found-content chariti-item-pdlr">
			<?php
				echo '<h1 class="chariti-not-found-head" >' . esc_html__('404', 'chariti') . '</h1>';
				echo '<h3 class="chariti-not-found-title chariti-content-font" >' . esc_html__('Page Not Found', 'chariti') . '</h3>';
				echo '<div class="chariti-not-found-caption" >' . esc_html__('Sorry, we couldn\'t find the page you\'re looking for.', 'chariti') . '</div>';

				echo '<form role="search" method="get" class="search-form" action="' . esc_url(home_url('/')) . '">';
				echo '<input type="text" class="search-field chariti-title-font" placeholder="' . esc_attr__('Type Keywords...', 'chariti') . '" value="" name="s">';
				echo '<div class="chariti-top-search-submit"><i class="fa fa-search" ></i></div>';
				echo '<input type="submit" class="search-submit" value="Search">';
				echo '</form>';
				echo '<div class="chariti-not-found-back-to-home" ><a href="' . esc_url(home_url('/')) . '" >' . esc_html__('Or Back To Homepage', 'chariti') . '</a></div>';
			?>
			</div>
		</div>
	</div>
	<?php

	get_footer(); 
