<?php
/**
 * The main template file
 */ 


	get_header();

	echo '<div class="chariti-content-container chariti-container">';
	echo '<div class="chariti-sidebar-style-none" >'; // for max width

	get_template_part('content/archive', 'default');

	echo '</div>'; // chariti-content-area
	echo '</div>'; // chariti-content-container

	get_footer(); 
