<?php
/**
 * The template for displaying archive not found
 */

	echo '<div class="chariti-not-found-wrap" id="chariti-full-no-header-wrap" >';
	echo '<div class="chariti-not-found-background" ></div>';
	echo '<div class="chariti-not-found-container chariti-container">';
	echo '<div class="chariti-header-transparent-substitute" ></div>';
	
	echo '<div class="chariti-not-found-content chariti-item-pdlr">';
	echo '<h1 class="chariti-not-found-head" >' . esc_html__('Not Found', 'chariti') . '</h1>';
	echo '<div class="chariti-not-found-caption" >' . esc_html__('Nothing matched your search criteria. Please try again with different keywords.', 'chariti') . '</div>';

	echo '<form role="search" method="get" class="search-form" action="' . esc_url(home_url('/')) . '">';
	echo '<input type="text" class="search-field chariti-title-font" placeholder="' . esc_attr__('Type Keywords...', 'chariti') . '" value="" name="s">';
	echo '<div class="chariti-top-search-submit"><i class="fa fa-search" ></i></div>';
	echo '<input type="submit" class="search-submit" value="Search">';
	echo '</form>';
	echo '<div class="chariti-not-found-back-to-home" ><a href="' . esc_url(home_url('/')) . '" >' . esc_html__('Or Back To Homepage', 'chariti') . '</a></div>';
	echo '</div>'; // chariti-not-found-content

	echo '</div>'; // chariti-not-found-container
	echo '</div>'; // chariti-not-found-wrap