<?php
/**
 * The template for displaying the footer
 */
	
	$post_option = chariti_get_post_option(get_the_ID());
	if( empty($post_option['enable-footer']) || $post_option['enable-footer'] == 'default' ){
		$enable_footer = chariti_get_option('general', 'enable-footer', 'enable');
	}else{
		$enable_footer = $post_option['enable-footer'];
	}	
	if( empty($post_option['enable-copyright']) || $post_option['enable-copyright'] == 'default' ){
		$enable_copyright = chariti_get_option('general', 'enable-copyright', 'enable');
	}else{
		$enable_copyright = $post_option['enable-copyright'];
	}

	$fixed_footer = chariti_get_option('general', 'fixed-footer', 'disable');
	echo '</div>'; // chariti-page-wrapper

	if( $enable_footer == 'enable' || $enable_copyright == 'enable' ){

		if( $fixed_footer == 'enable' ){
			echo '</div>'; // chariti-body-wrapper

			echo '<footer class="chariti-fixed-footer" id="chariti-fixed-footer" >';
		}else{
			echo '<footer>';
		}

		if( $enable_footer == 'enable' ){

			$chariti_footer_layout = array(
				'footer-1'=>array('chariti-column-60'),
				'footer-2'=>array('chariti-column-15', 'chariti-column-15', 'chariti-column-15', 'chariti-column-15'),
				'footer-3'=>array('chariti-column-15', 'chariti-column-15', 'chariti-column-30',),
				'footer-4'=>array('chariti-column-20', 'chariti-column-20', 'chariti-column-20'),
				'footer-5'=>array('chariti-column-20', 'chariti-column-40'),
				'footer-6'=>array('chariti-column-40', 'chariti-column-20'),
			);
			$footer_style = chariti_get_option('general', 'footer-style');
			$footer_style = empty($footer_style)? 'footer-2': $footer_style;

			$count = 0;
			$has_widget = false;
			foreach( $chariti_footer_layout[$footer_style] as $layout ){ $count++;
				if( is_active_sidebar('footer-' . $count) ){ $has_widget = true; }
			}

			if( $has_widget ){ 	

				$footer_column_divider = chariti_get_option('general', 'enable-footer-column-divider', 'enable');
				$extra_class  = ($footer_column_divider == 'enable')? ' chariti-with-column-divider': '';

				echo '<div class="chariti-footer-wrapper ' . esc_attr($extra_class) . '" >';
				echo '<div class="chariti-footer-container chariti-container clearfix" >';
				
				$count = 0;
				foreach( $chariti_footer_layout[$footer_style] as $layout ){ $count++;
					echo '<div class="chariti-footer-column chariti-item-pdlr ' . esc_attr($layout) . '" >';
					if( is_active_sidebar('footer-' . $count) ){
						dynamic_sidebar('footer-' . $count); 
					}
					echo '</div>';
				}
				
				echo '</div>'; // chariti-footer-container
				echo '</div>'; // chariti-footer-wrapper 
			}
		} // enable footer

		if( $enable_copyright == 'enable' ){
			$copyright_style = chariti_get_option('general', 'copyright-style', 'center');
			
			if( $copyright_style == 'center' ){
				$copyright_text = chariti_get_option('general', 'copyright-text');

				if( !empty($copyright_text) ){
					echo '<div class="chariti-copyright-wrapper" >';
					echo '<div class="chariti-copyright-container chariti-container">';
					echo '<div class="chariti-copyright-text chariti-item-pdlr">';
					echo gdlr_core_escape_content(gdlr_core_text_filter($copyright_text));
					echo '</div>';
					echo '</div>';
					echo '</div>'; // chariti-copyright-wrapper
				}
			}else if( $copyright_style == 'left-right' ){
				$copyright_left = chariti_get_option('general', 'copyright-left');
				$copyright_right = chariti_get_option('general', 'copyright-right');

				if( !empty($copyright_left) || !empty($copyright_right) ){
					echo '<div class="chariti-copyright-wrapper" >';
					echo '<div class="chariti-copyright-container chariti-container clearfix">';
					if( !empty($copyright_left) ){
						echo '<div class="chariti-copyright-left chariti-item-pdlr">';
						echo gdlr_core_escape_content(gdlr_core_text_filter($copyright_left));
						echo '</div>';
					}

					if( !empty($copyright_right) ){
						echo '<div class="chariti-copyright-right chariti-item-pdlr">';
						echo gdlr_core_escape_content(gdlr_core_text_filter($copyright_right));
						echo '</div>';
					}
					echo '</div>';
					echo '</div>'; // chariti-copyright-wrapper
				}
			}
		}

		echo '</footer>';

		if( $fixed_footer == 'disable' ){
			echo '</div>'; // chariti-body-wrapper
		}
		echo '</div>'; // chariti-body-outer-wrapper

	// disable footer	
	}else{
		echo '</div>'; // chariti-body-wrapper
		echo '</div>'; // chariti-body-outer-wrapper
	}

	$header_style = chariti_get_option('general', 'header-style', 'plain');
	
	if( $header_style == 'side' || $header_style == 'side-toggle' ){
		echo '</div>'; // chariti-header-side-nav-content
	}

	$back_to_top = chariti_get_option('general', 'enable-back-to-top', 'disable');
	if( $back_to_top == 'enable' ){
		echo '<a href="#chariti-top-anchor" class="chariti-footer-back-to-top-button" id="chariti-footer-back-to-top-button"><i class="fa fa-angle-up" ></i></a>';
	}
?>

<?php wp_footer(); ?>

</body>
</html>