<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'rmbhs-wp' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '.t:#OtI2+N/A<PF,<U,ZV(Zzlha*IJQ:$ay*xy2{okiH:U%TH&Ur?)p^Mnn}V/gc' );
define( 'SECURE_AUTH_KEY',  'hG|@Qce1y3<~ggX^T0N9tu1xqY;{FPt+7cLJQZ9,+v@zXm9<){7auljHL_eeSP`>' );
define( 'LOGGED_IN_KEY',    '$1_XUrMb@>8:m,O$RWUQ+U4SEyk|iVe(,7R%^s&?j~9~M*fd Q8h1%t8,J?jE{/C' );
define( 'NONCE_KEY',        'Nl0{*gb4I=xC<EO[&1MdBQ9/CW![G^^mZF;cT >qxt)2$rQ(3jhw_EU!49[xCXhz' );
define( 'AUTH_SALT',        '_$KQQ&Cs$#TO9/y:D<;a: ~7~F(H*^&uFMQ5@R(l,;{t.eY,lboI@xAW<7roIHsU' );
define( 'SECURE_AUTH_SALT', '3ETN!}x/Y^:B~:H`OqRUhH s8$U``7wt=l/`qgR[|k4,QYk%F#NALsnJ;mMGFxam' );
define( 'LOGGED_IN_SALT',   'hBAu(Uu@Cwu]{{rN_nuY[AqIWAL/j@`J(</qBCwX,S9E%W@gy-T@^ktd;dch+#,E' );
define( 'NONCE_SALT',       'x/bH`HJox]k.t+2npB=6gBxX0P]O;.G5t8RV<3`$ YSBzx0,Y5f!_>Jd?:1Q=p{^' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
